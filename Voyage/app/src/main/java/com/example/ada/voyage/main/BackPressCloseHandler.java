package com.example.ada.voyage.main;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: When the user clicks "Back" button on his mobile device,
 * the toast message asks the user to click the "Back" button once again if they want to exit from the application.
 * Briefly, the user need to click "Back" button twice to exit from the application.
 */

public class BackPressCloseHandler {
    private long backKeyPressedTime = 0;
    private Toast toast;
    private Activity activity;

    // Constructor
    public BackPressCloseHandler(Activity context) {
        this.activity = context;
    }

    /**
     * If the user clicks "Back" button,
     * 1. Shows message to click again to exit.
     * 2. Checks again whether they clicked in a certain time limit or not.
     * If the user clicks the "Back" button again on time, then exit;
     * otherwise, do not exit from the program.
     */
    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            activity.finish();
            toast.cancel();
        }
    }

    // Toast message method
    public void showGuide() {
        toast = Toast.makeText(activity, "Press back button again to exit.", Toast.LENGTH_SHORT);
        toast.show();
    }
}
