package com.example.ada.voyage.budget;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.ada.voyage.budget.TripDBHelper.COLUMN_TRIP_ID;
import static com.example.ada.voyage.budget.TripDBHelper.COLUMN_TRIP_LEFT;
import static com.example.ada.voyage.budget.TripDBHelper.COLUMN_TRIP_SPENT;
import static com.example.ada.voyage.budget.TripDBHelper.COLUMN_TRIP_TITLE;

/**
 * Created by: Jiyoon Lim
 * Modified by: Jiwon Choi (DB)
 * Description: Displays the list of trip that the user added.
 *  * The user can add, edit or remove the trip
 */

public class BudgetFragment extends Fragment {
    protected static TripDBHelper tripDBHelper; // TripDBHelper declaration
    protected static ArrayList<Trip> tripList = new ArrayList<>();
    protected static TripAdapter adapter;
    protected static CategoryDBHelper categoryDBHelper;
    protected static MoneyDBHelper moneyDBHelper;
//    private Trip selected_item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget, null);

        tripDBHelper = new TripDBHelper(this.getActivity(), null, null, 1); // TripDBHelper declaration
        categoryDBHelper = new CategoryDBHelper(this.getActivity(), null, null, 1);
        moneyDBHelper = new MoneyDBHelper(this.getActivity(), null, null,1);
        adapter = new TripAdapter(getContext(), tripList);
        ListView listview = (ListView) view.findViewById(R.id.trip_list);
        listview.setAdapter(adapter);

        //when clicking one of item in the list, go to trip detail with certain information
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView,
                                    View view, int position, long id) {
                Intent intent = new Intent(BudgetFragment.this.getActivity(), TripDetailActivity.class);
                intent.putExtra("trip detail position", position);
                intent.putExtra("trip detail id", tripList.get(position).getId());
                startActivity(intent);
            }
        });

        //when long-clicking one of item in the list,
        // show dialog with saved title to edit or remove
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder editRemoveTrip = new AlertDialog.Builder(getActivity());
                editRemoveTrip.setTitle("Edit or remove the trip title");

                //editText
                final EditText et = new EditText(getActivity());
                et.setText(tripList.get(position).getTitle());
                editRemoveTrip.setView(et);//end of editText

                editRemoveTrip.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(et.getText().toString().length()==0){
                            Toast.makeText(getActivity(), "Enter a trip title first", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Trip trip = tripList.get(position);
                            trip.setTitle(et.getText().toString());
                            tripDBHelper.editTrip(trip);
                            tripList.set(position, trip);
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }
                });
                editRemoveTrip.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                editRemoveTrip.setNegativeButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Trip trip = tripList.get(position);
                        categoryDBHelper.deleteCategoryFromTrip(trip.getId());
                        moneyDBHelper.deleteMoneyFromTrip(trip.getId());
                        tripDBHelper.deleteTrip(trip);
                        tripList.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                editRemoveTrip.show();
                return true;
            }
        });
        //end of listView

        //When clicking a floating button to add a new trip in budget fragment
        FloatingActionButton addTripButton;
        addTripButton = (FloatingActionButton) view.findViewById(R.id.add_trip_button);
        addTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //alertDialog to add a new trip on the list
                AlertDialog.Builder addTrip = new AlertDialog.Builder(getActivity());
                addTrip.setTitle("Add a new trip");
                //editText to enter a new trip title
                final EditText et = new EditText(getActivity());
                addTrip.setView(et); //end of editText
                addTrip.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(et.getText().toString().length()==0){
                            Toast.makeText(getActivity(), "Enter a trip title first", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            //add a new trip in the trip list
                            int tempId = -1;
                            try {
                                JSONArray array = tripDBHelper.viewTrip();
                                if (array.length() == 0) {
                                    tempId = 1;
                                } else {
                                    JSONObject obj = (JSONObject) array.get(array.length() - 1);
                                    tempId = obj.getInt(COLUMN_TRIP_ID) + 1;
                                }
                            } catch (JSONException e) {
                                Toast.makeText(getActivity(), "Failed to load database", Toast.LENGTH_LONG).show();
                            }
                            Trip trip = new Trip (et.getText().toString(), "0", "0");
                            tripDBHelper.addTrip(trip);
                            tripList.add(trip);
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }
                });
                addTrip.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                //When clicking floating button, pop up dialog to add a new trip with title
                addTrip.show();
            }
        }); //end of floating button
        return view;
    }

    //it runs when showing this fragment
    @Override
    public void onResume() {
        super.onResume();
        //get data from database and view them on the list
        adapter.clear();
        try {
            JSONArray array = tripDBHelper.viewTrip();
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                adapter.add(new Trip(obj.getInt(COLUMN_TRIP_ID),
                        obj.get(COLUMN_TRIP_TITLE).toString(),
                        obj.get(COLUMN_TRIP_LEFT).toString(),
                        obj.get(COLUMN_TRIP_SPENT).toString()
                ));
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Failed to load database", Toast.LENGTH_LONG).show();
        }
    }
}