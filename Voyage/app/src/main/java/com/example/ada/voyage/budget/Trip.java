package com.example.ada.voyage.budget;

/**
 * Created by: Jiyoon Lim
 * Description: Trip with id, title and total money spent and left.
 */

public class Trip {
    private int id = 1;
    private String title;
    private String moneyLeft;
    private String moneySpent;
    private static int lastID = 1;

    public Trip(int id, String title, String moneyLeft, String moneySpent) {
        this.id = id;
        this.title = title;
        this.moneyLeft = moneyLeft;
        this.moneySpent = moneySpent;
        if(id > lastID) {
            lastID = id;
        }
        else if(id == lastID) {
            lastID ++;
        }
    }

    public Trip(String title, String moneyLeft, String moneySpent) {
        this.id = lastID;
        this.title = title;
        this.moneyLeft = moneyLeft;
        this.moneySpent = moneySpent;
        lastID ++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoneyLeft() {
        return moneyLeft;
    }

    public void setMoneyLeft(String moneyLeft) {
        this.moneyLeft = moneyLeft;
    }

    public String getMoneySpent() {
        return moneySpent;
    }

    public void setMoneySpent(String moneySpent) {
        this.moneySpent = moneySpent;
    }
}
