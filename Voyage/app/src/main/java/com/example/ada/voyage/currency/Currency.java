package com.example.ada.voyage.currency;

/**
 * Created by YongHo on 2017. 5. 23..
 */

public class Currency {

    private String name1;
    private int flag;
    private double rate;

    public Currency(String name1, int flag, double rate) {
        this.name1 = name1;
        this.flag = flag;
        this.rate = rate;
    }

    public String getName1() {
        return name1;
    }

    public int getFlag() {return flag;}

    public double getRate() {
        return rate;
    }

}
