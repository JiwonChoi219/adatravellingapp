package com.example.ada.voyage.blog;

import android.content.Context;
import android.net.Uri;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ada.voyage.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.ada.voyage.blog.BlogFragment.cardAdapter;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: Custom card adapter for customized card view.
 */

public class CardAdapter extends BaseAdapter {
    protected ArrayList<Card> cardList;
    private Context context;

    // Constructor
    public CardAdapter(Context context, ArrayList<Card> list) {
        this.context = context;
        this.cardList = list;
    }

    /**
     * Inner class: CardViewHolder
     */
    class CardViewHolder {
        protected FrameLayout images;
        protected TextView titleTextView;
        protected TextView dateTextView;
        protected ImageView coverImageView1_1;
        protected ImageView coverImageView2_1;
        protected ImageView coverImageView2_2;
        protected ImageView coverImageView3_1;
        protected ImageView coverImageView3_2;
        protected ImageView coverImageView3_3;
        protected TextView contentTextView;
        protected TextView moreSign;
    }

    // Remove everything from the ArrayList.
    public void clear() {
        cardList.clear();
    }

    // Add a Card item to the ArrayList and update to the Adapter.
    public void add(Card c) {
        cardList.add(c);
        cardAdapter.notifyDataSetChanged();
    }

    // Remove a Card item to the ArrayList and update to the Adapter.
    public void remove(Card c) {
        cardList.remove(c);
        cardAdapter.notifyDataSetChanged();
    }

    // Edit a Card item to the ArrayList and update to the Adapter.
    public void edit (int p, Card c) {
        cardList.set(p, c);
        cardAdapter.notifyDataSetChanged();
    }

    private ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<int[]>();

        Pattern pattern = Pattern.compile(prefix + "\\w+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    /**
     * Link the cardview to the xml.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.blog_cardview, parent, false);

            CardViewHolder holder = new CardViewHolder();
            holder.titleTextView = (TextView) convertView.findViewById(R.id.card_title);
            holder.dateTextView = (TextView) convertView.findViewById(R.id.card_date);
            holder.images = (FrameLayout) convertView.findViewById(R.id.card_images);
            holder.coverImageView1_1 = (ImageView) convertView.findViewById(R.id.card_1_image);
            holder.coverImageView2_1 = (ImageView) convertView.findViewById(R.id.card_2_image_1);
            holder.coverImageView2_2 = (ImageView) convertView.findViewById(R.id.card_2_image_2);
            holder.coverImageView3_1 = (ImageView) convertView.findViewById(R.id.card_3_image_1);
            holder.coverImageView3_2 = (ImageView) convertView.findViewById(R.id.card_3_image_2);
            holder.coverImageView3_3 = (ImageView) convertView.findViewById(R.id.card_3_image_3);
            holder.contentTextView = (TextView) convertView.findViewById(R.id.card_content);
            holder.moreSign = (TextView) convertView.findViewById(R.id.card_more);

            convertView.setTag(holder);
        }

        Card post = cardList.get(position);

        if (post != null) {
            CardViewHolder holder = (CardViewHolder) convertView.getTag();
            holder.titleTextView.setText(post.getCardTitle());
            holder.dateTextView.setText(post.getCardDate());

            /**
             * To put textlink and color on the hashtagged word.
             */
            ArrayList<int[]> hashtagSpans = getSpans(post.getCardContent(), '#');
            SpannableString spannableString =
                    new SpannableString(this.cardList.get(position).getCardContent());

            for (int i = 0; i < hashtagSpans.size(); i++) {
                int[] span = hashtagSpans.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                spannableString.setSpan(new Hashtag(context), hashTagStart, hashTagEnd, 0);
            }
            holder.contentTextView.setText(spannableString);

            /**
             * Depending on the number of posts, it links the image to the imageview layout.
             */
            if (checkImageExistence(post) == 0) {
                holder.images.setVisibility(View.GONE);
            } else if (checkImageExistence(post) == 1) {
                Uri temp = Uri.parse(post.getImageResource1());
                Picasso.with(context).load(Uri.parse(post.getImageResource1())).fit().centerCrop().into(holder.coverImageView1_1);
                holder.coverImageView1_1.bringToFront();

                holder.coverImageView1_1.setVisibility(View.VISIBLE);
                holder.images.setVisibility(View.VISIBLE);

                holder.coverImageView2_1.setVisibility(View.GONE);
                holder.coverImageView2_2.setVisibility(View.GONE);
                holder.coverImageView3_1.setVisibility(View.GONE);
                holder.coverImageView3_2.setVisibility(View.GONE);
                holder.coverImageView3_3.setVisibility(View.GONE);
                holder.moreSign.setVisibility(View.GONE);

            } else if (checkImageExistence(post) == 2) {
                Uri temp1 = Uri.parse(post.getImageResource1());
                Picasso.with(context).load(temp1).fit().centerCrop().into(holder.coverImageView2_1);

                Uri temp2 = Uri.parse(post.getImageResource2());
                Picasso.with(context).load(temp2).fit().centerCrop().into(holder.coverImageView2_2);

                holder.coverImageView2_1.bringToFront();
                holder.coverImageView2_2.bringToFront();

                holder.coverImageView2_1.setVisibility(View.VISIBLE);
                holder.coverImageView2_2.setVisibility(View.VISIBLE);
                holder.images.setVisibility(View.VISIBLE);

                holder.coverImageView1_1.setVisibility(View.GONE);
                holder.coverImageView3_1.setVisibility(View.GONE);
                holder.coverImageView3_2.setVisibility(View.GONE);
                holder.coverImageView3_3.setVisibility(View.GONE);
                holder.moreSign.setVisibility(View.GONE);

            } else if (checkImageExistence(post) == 3) {
                Uri temp1 = Uri.parse(post.getImageResource1());
                Picasso.with(context).load(temp1).fit().centerCrop().into(holder.coverImageView3_1);

                Uri temp2 = Uri.parse(post.getImageResource2());
                Picasso.with(context).load(temp2).fit().centerCrop().into(holder.coverImageView3_2);

                Uri temp3 = Uri.parse(post.getImageResource3());
                Picasso.with(context).load(temp3).fit().centerCrop().into(holder.coverImageView3_3);

                holder.coverImageView3_1.setVisibility(View.VISIBLE);
                holder.coverImageView3_2.setVisibility(View.VISIBLE);
                holder.coverImageView3_3.setVisibility(View.VISIBLE);
                holder.images.setVisibility(View.VISIBLE);

                holder.coverImageView1_1.setVisibility(View.GONE);
                holder.coverImageView2_1.setVisibility(View.GONE);
                holder.coverImageView2_2.setVisibility(View.GONE);
                holder.moreSign.setVisibility(View.GONE);

            } else if (checkImageExistence(post) == 4) {
                Uri temp1 = Uri.parse(post.getImageResource1());
                Picasso.with(context).load(temp1).fit().centerCrop().into(holder.coverImageView3_1);

                Uri temp2 = Uri.parse(post.getImageResource2());
                Picasso.with(context).load(temp2).fit().centerCrop().into(holder.coverImageView3_2);

                Uri temp3 = Uri.parse(post.getImageResource3());
                Picasso.with(context).load(temp3).fit().centerCrop().into(holder.coverImageView3_3);

                holder.coverImageView3_1.setVisibility(View.VISIBLE);
                holder.coverImageView3_2.setVisibility(View.VISIBLE);
                holder.coverImageView3_3.setVisibility(View.VISIBLE);
                holder.images.setVisibility(View.VISIBLE);

                holder.coverImageView1_1.setVisibility(View.GONE);
                holder.coverImageView2_1.setVisibility(View.GONE);
                holder.coverImageView2_2.setVisibility(View.GONE);

                holder.moreSign.setVisibility(View.VISIBLE);
                holder.moreSign.setText("+1");
                holder.moreSign.bringToFront();
            } else if (checkImageExistence(post) == 5) {
                Uri temp1 = Uri.parse(post.getImageResource1());
                Picasso.with(context).load(temp1).fit().centerCrop().into(holder.coverImageView3_1);

                Uri temp2 = Uri.parse(post.getImageResource2());
                Picasso.with(context).load(temp2).fit().centerCrop().into(holder.coverImageView3_2);

                Uri temp3 = Uri.parse(post.getImageResource3());
                Picasso.with(context).load(temp3).fit().centerCrop().into(holder.coverImageView3_3);

                holder.coverImageView3_1.setVisibility(View.VISIBLE);
                holder.coverImageView3_2.setVisibility(View.VISIBLE);
                holder.coverImageView3_3.setVisibility(View.VISIBLE);
                holder.images.setVisibility(View.VISIBLE);

                holder.coverImageView1_1.setVisibility(View.GONE);
                holder.coverImageView2_1.setVisibility(View.GONE);
                holder.coverImageView2_2.setVisibility(View.GONE);

                holder.moreSign.setVisibility(View.VISIBLE);
                holder.moreSign.setText("+2");
                holder.moreSign.bringToFront();
            }

        }

        return convertView;
    }

    /**
     * Check the number of images that are currently stored in one blog post (Card object).
     * @param post
     * @return
     */
    private int checkImageExistence (Card post) {
        int imgCount = 0;
        if (post.getImageResource1().equals("Exception: No Image Applied")) {
            imgCount++;
        }
        if (post.getImageResource2().equals("Exception: No Image Applied")) {
            imgCount++;
        }
        if (post.getImageResource3().equals("Exception: No Image Applied")) {
            imgCount++;
        }
        if (post.getImageResource4().equals("Exception: No Image Applied")) {
            imgCount++;
        }
        if (post.getImageResource5().equals("Exception: No Image Applied")) {
            imgCount++;
        }

        return (5 - imgCount);
    }

    @Override
    public int getCount() {
        return cardList.size();
    }

    @Override
    public Object getItem(int position) {
        return cardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cardList.get(position).hashCode();
    }

}
