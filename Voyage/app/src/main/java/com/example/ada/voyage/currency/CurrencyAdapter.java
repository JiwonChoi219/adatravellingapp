package com.example.ada.voyage.currency;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ada.voyage.R;

import java.util.List;

/**
 * Created by YongHo on 2017. 5. 23..
 */

public class CurrencyAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Currency> currencyList;
    private CurrencyItemClickListener currencyItemClickListener;


    public CurrencyAdapter(Context context, List<Currency> currencyList, CurrencyItemClickListener currencyItemClickListener) {
        this.context = context;
        this.currencyList = currencyList;
        this.currencyItemClickListener = currencyItemClickListener;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return currencyList.size();
    }

    @Override
    public Object getItem(int i) {
        return currencyList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /** Receive name, rate, flag information to display in the list view for the first activity */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View currencyItemView = layoutInflater.inflate(R.layout.currency_item, null);
        TextView tvName = (TextView) currencyItemView.findViewById(R.id.tvName);
        TextView tvRate = (TextView) currencyItemView.findViewById(R.id.tvRate);
        ImageView flags = (ImageView) currencyItemView.findViewById(R.id.FlagImage);

        final Currency c = currencyList.get(i);
        tvName.setText(c.getName1());
        tvRate.setText(Double.toString(c.getRate()));
        flags.setImageResource(c.getFlag());

        currencyItemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                currencyItemClickListener.onCurrencyItemClick(c, view);
            }
        });
        return currencyItemView;
    }
}
