package com.example.ada.voyage.currency;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by YongHo on 2017. 5. 23..
 */

public interface CurrencyExchangeService {
    //Receive currency rates that are latest from Fixor.io HTTP call
    @GET("latest")
    Call<CurrencyExchange> loadCurrencyExchange();
}
