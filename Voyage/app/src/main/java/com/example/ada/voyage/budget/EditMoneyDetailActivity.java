package com.example.ada.voyage.budget;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.ada.voyage.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.example.ada.voyage.budget.CategoryDetailActivity.mAdapter;
import static com.example.ada.voyage.budget.CategoryDetailActivity.moneyDBHelper;
import static com.example.ada.voyage.budget.CategoryDetailActivity.moneyList;

/**
 * Created by: Jiyoon Lim
 * Modified by: Jiwon Choi (DB)
 * Description: edit money in a certain category which the user click
 */

public class EditMoneyDetailActivity extends AppCompatActivity {

    EditText editTitle;
    EditText editAmount;
    EditText editDate;
    RadioButton editPositiveButton;
    RadioButton editNegativeButton;
    RadioGroup amountTypeRadioButton;
    int year, month, day, position;
    String sign;
    private int moneyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_detail);

        editTitle = (EditText) findViewById(R.id.money_detail_title);
        editAmount = (EditText) findViewById(R.id.money_detail_amount);
        editDate = (EditText) findViewById(R.id.money_detail_date);
        editNegativeButton = (RadioButton) findViewById(R.id.money_detail_negative_radiobutton);
        editPositiveButton = (RadioButton) findViewById(R.id.money_detail_postive_radiobutton);
        amountTypeRadioButton = (RadioGroup) findViewById(R.id.money_detail_radiogroup);

        Intent intent = getIntent();
        position = intent.getIntExtra("edit money position", -1);
        Money money = moneyList.get(position);
        editTitle.setText(money.getTitle());
        editDate.setText(money.getDate());
        editAmount.setText(money.getAmount());
        sign = money.getSign();

        moneyID = intent.getIntExtra("edit money id", -1);
        if (moneyID < 0) {
            Toast.makeText(this, "Error in database.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        position = intent.getIntExtra("edit money position", -1);
        if (sign.equals("+")) {
            editPositiveButton.setChecked(true);
        } else {//sign.equals("-")
            editNegativeButton.setChecked(true);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Edit the money");
        actionBar.setDisplayHomeAsUpEnabled(true);


        GregorianCalendar calendar = new GregorianCalendar();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditMoneyDetailActivity.this, dateSetListener, year, month, day).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_action_bar, menu);
        return true;
    }

    //edit money: make a new money according to input which the user enters
    // and replace it with the original money which the user changes.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bar_cancel:
                finish();
                break;
            case R.id.action_bar_save:
                Intent intent = getIntent();
                int position = intent.getIntExtra("edit money position", -1);

                String dateStr = editDate.getText().toString();
                String amountStr = editAmount.getText().toString();
                String titleStr = editTitle.getText().toString();
                String editSign;
                amountTypeRadioButton = (RadioGroup) findViewById(R.id.money_detail_radiogroup);
                int checkedRadioID = amountTypeRadioButton.getCheckedRadioButtonId();

                if (dateStr.length() == 0 || amountStr.length() == 0 || titleStr.length() == 0) {
                    Toast.makeText(EditMoneyDetailActivity.this, "Fill out the all blanks first", Toast.LENGTH_SHORT).show();
                } else {
                    Money money = moneyList.get(position);
                    if (checkedRadioID == R.id.money_detail_negative_radiobutton) {
                        editSign = "-";

                        money.setTitle(titleStr);
                        money.setDate(dateStr);
                        money.setAmount(amountStr);
                        money.setSign(editSign);

                        moneyDBHelper.editMoney(money);
                        moneyList.set(position, money);
//                        mAdapter.edit(position, money);
                        mAdapter.notifyDataSetChanged();
                        finish();
                    } else if (checkedRadioID == R.id.money_detail_postive_radiobutton) {
                        editSign = "+";

                        money.setTitle(titleStr);
                        money.setDate(dateStr);
                        money.setAmount(amountStr);
                        money.setSign(editSign);

                        moneyDBHelper.editMoney(money);
                        moneyList.set(position, money);
//                        mAdapter.edit(position, money);
                        mAdapter.notifyDataSetChanged();
                        finish();
                    } else {
                        Toast.makeText(EditMoneyDetailActivity.this, "Check a type of amount first", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //back icon
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    // Date picker dialog
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg = String.format("%d/%d/%d", monthOfYear + 1, dayOfMonth, year);

            if (monthOfYear < 10 || dayOfMonth < 10) {
                String month = "", day = "";

                if (monthOfYear < 10) {
                    month = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    month = String.valueOf(monthOfYear + 1);
                }

                if (dayOfMonth < 10) {
                    day = "0" + String.valueOf(dayOfMonth);
                } else {
                    day = String.valueOf(dayOfMonth);
                }

                msg = String.format("%s/%s/%d", month, day, year);
            }

            editDate.setText(msg);
        }
    };
}
