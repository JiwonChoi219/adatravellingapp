package com.example.ada.voyage.blog;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ada.voyage.R;
import com.example.ada.voyage.main.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_CONTENT;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_DATE;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_ID_BLOG;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_1;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_2;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_3;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_4;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_5;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_TITLE_BLOG;

/**
 * Created by: Jiwon Choi
 * Modified by: YongHo Son (Permission)
 * Description: Displays the preview/overview of blog posts that the user posted.
 */

public class BlogFragment extends Fragment {
    private FloatingActionButton addPost;
    public static BlogDBHelper bDBHelper;
    protected static ArrayList<Card> cardList = new ArrayList<>();
    protected static ListView listView;
    protected static CardAdapter cardAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Blog");

        /**
         * Modified by: YongHo Son
         * Check permission request
         */
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }

        // attribute declarations
        bDBHelper = new BlogDBHelper(this.getActivity(), null, null, 1);
        listView = (ListView) view.findViewById(R.id.card_list);
        cardAdapter = new CardAdapter(getContext(), cardList);
        listView.setAdapter(cardAdapter);

        /**
         * If the user clicks "Add Post" floating action button, then he will move on to the new Activity,
         * where he can add a new blog post.
         */
        addPost = (FloatingActionButton) view.findViewById(R.id.add_post_button);
        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BlogFragment.this.getActivity(), AddPostActivity.class);
                startActivity(intent);
            }
        });

        /**
         * If the user clicks a certain card(post), then he will move on to the new Activity,
         * where he can view the details of the blog post.
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(BlogFragment.this.getActivity(), ViewPostActivity.class);

                intent.putExtra("Post Title", cardList.get(position).getCardTitle());
                intent.putExtra("Post Date", cardList.get(position).getCardDate());
                intent.putExtra("Post Content", cardList.get(position).getCardContent());
                intent.putExtra("Post ID", cardList.get(position).getCardId());
                intent.putExtra("Post Position", position);

                startActivity(intent);
            }
        });

        return view;
    }

    /**
     * Get the list of blog posts from the recent ones.
     */
    @Override
    public void onResume() {
        super.onResume();
        cardAdapter.clear();
        try {
            JSONArray array = bDBHelper.viewPost();
            for (int i = array.length() - 1; i >= 0; i--) {
                JSONObject obj = (JSONObject) array.get(i);
                cardAdapter.add(new Card(obj.getInt(COLUMN_ID_BLOG),
                        obj.get(COLUMN_TITLE_BLOG).toString(),
                        obj.get(COLUMN_DATE).toString(),
                        obj.get(COLUMN_IMG_1).toString(),
                        obj.get(COLUMN_IMG_2).toString(),
                        obj.get(COLUMN_IMG_3).toString(),
                        obj.get(COLUMN_IMG_4).toString(),
                        obj.get(COLUMN_IMG_5).toString(),
                        obj.get(COLUMN_CONTENT).toString()
                ));
            }
            cardAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Failed to load database", Toast.LENGTH_LONG).show();
        }
    }

}
