package com.example.ada.voyage.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ada.voyage.countryInfo.CountryInfoFragment;
import com.example.ada.voyage.currency.CurrencyFragment;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ada.voyage.map.MapFragment;
import com.example.ada.voyage.R;
import com.example.ada.voyage.blog.BlogFragment;
import com.example.ada.voyage.budget.BudgetFragment;
import com.example.ada.voyage.calendar.CalendarFragment;
import com.example.ada.voyage.weather.WeatherFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

/**
 * Created by: Jiwon Choi
 * Modified by: YongHo Son (GPS)
 * Description: Switch the fragment(screen) by the navigation drawer
 * Another modification by: Jaebin Yang (db creation in firebase),
 *                          username/icon/user email showing in drawer, enable settings
*/

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Context context;
    //private ListView lvCurrency;
    private BackPressCloseHandler backPressCloseHandler;
    View header;
    FirebaseDatabase database;
    DatabaseReference myRef;
    FirebaseAuth mAuth;
    ImageButton setting;
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backPressCloseHandler = new BackPressCloseHandler(this);

        /** Jaebin Yang*/
        /** Create a new Firebase database for designated user (if there isn't one already */
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("/users");
        mAuth = FirebaseAuth.getInstance();
        myRef.push().setValue("users");
        FirebaseUser currentUser =  mAuth.getCurrentUser();
        String personName = (String) currentUser.getDisplayName();
        String personEmail = (String) currentUser.getEmail();
        String personId = (String) currentUser.getUid();
        Uri photoUrl = currentUser.getPhotoUrl();
        DatabaseReference childRef = myRef.child(personId);
        DatabaseReference child1 = childRef.child("Email");
        DatabaseReference child2 = childRef.child("Name");

        child1.setValue(personEmail);
        child2.setValue(personName);



        //lvCurrency = (ListView) findViewById(R.id.lvcurrency);


        /** Jiwon Choi */
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        /** Jaebin Yang */
        /** For the navigation drawer header, show the user name, user email account,
         * and the user's profile pic.
         */
        header = navigationView.getHeaderView(0);
        TextView username = (TextView) header.findViewById(R.id.username);
        TextView userEmail = (TextView) header.findViewById(R.id.userAccount);
        ImageView profile = (ImageView) header.findViewById(R.id.imageView);
        username.setText(personName);
        userEmail.setText(personEmail);
        Picasso.with(MainActivity.this).load(photoUrl).into(profile);

        navigationView.setNavigationItemSelectedListener(this);
        displaySelectedScreen(R.id.nav_blog);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            backPressCloseHandler.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void displaySelectedScreen(int itemId) {
        ActionBar actionBar = getSupportActionBar();
        //creating fragment object
        Fragment fragment = null;

        setting = (ImageButton) header.findViewById(R.id.settings);

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_blog:
                actionBar.setTitle("Blog");
                fragment = new BlogFragment();
                break;
            case R.id.nav_calendar:
                actionBar.setTitle("Calendar");
                fragment = new CalendarFragment();
                break;
            case R.id.nav_currency:
                actionBar.setTitle("Currency Converter");
                fragment = new CurrencyFragment();
                break;
            case R.id.nav_map:
                /*
                 * Modified by: YongHo Son
                 * Display the screen of turning on GPS manually if GPS is not turned on before using map function
                 */
                String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                    if (!provider.contains("gps")) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        Toast.makeText(getApplicationContext(), "Please Turn on GPS", Toast.LENGTH_LONG).show();
                    }
                    if (provider.contains("gps")) {
                        actionBar.setTitle("Map");
                        fragment = new MapFragment();
                     }
                break;
            case R.id.nav_budget:
                actionBar.setTitle("Budget");
                fragment = new BudgetFragment();
                break;
            case R.id.nav_countryInfo:
                actionBar.setTitle("Country Information");
                fragment = new CountryInfoFragment();
                break;
            case R.id.nav_weather:
                actionBar.setTitle("Weather");
                fragment = new WeatherFragment();
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    /** Jaebin Yang */
    public void onClick(View view) {
        /** If the user clicks the settings icon, go to the settings activity after closing the drawer */
        if(view.getId()==(header.findViewById(R.id.settings)).getId()){
            Intent i= new Intent(this, SettingsActivity.class);
            startActivity(i);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

}
