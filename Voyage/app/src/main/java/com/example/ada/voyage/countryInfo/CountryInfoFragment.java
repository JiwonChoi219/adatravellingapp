package com.example.ada.voyage.countryInfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ada.voyage.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class CountryInfoFragment extends Fragment {
    protected EditText et;
    protected Button btn;
    protected static URL url;
    protected WebView wv;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_countryinfo, null);

        et = (EditText) view.findViewById(R.id.counrty_search_text);
        btn = (Button) view.findViewById(R.id.country_search_btn);
        wv = (WebView) view.findViewById(R.id.country_web);

        Browser mBrowser = new Browser();
        wv.setWebViewClient(mBrowser);

        WebSettings mWebSetting = wv.getSettings();
        mWebSetting.setLoadsImagesAutomatically(true);
        mWebSetting.setJavaScriptEnabled(true);

        //automatically recommend places according to input
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    Toast.makeText(getActivity(), "Exception", Toast.LENGTH_SHORT).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(getActivity(), "Exception", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //When clicking a search button, web view is shown according to url.
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String countryName = et.getText().toString();

                //converts countryName which the user enters in editText into valid countryName to search
                //(e.g. "South Korea -> "South_Korea")
                String str = "";
                for (int i = 0; i<countryName.length(); i++){
                    char cn = countryName.charAt(i);
                    if(cn == ' '){
                        str += "_";
                    } else {
                        str += cn;
                    }
                }
                countryName = str;
                //end of converting

                if(countryName.length()==0){
                    Toast.makeText(getActivity(), "enter a country first", Toast.LENGTH_SHORT).show();
                } else{
                    try {
                        String url = "https://en.m.wikivoyage.org/wiki/"
                                + URLEncoder.encode(countryName, "UTF-8");
                        wv.loadUrl(url);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return view;
    }

    //automatically recommend places according to input
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.i("WeatherFragment", "Place: " + place.getName());

                et.setText(place.getName());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.i("WeatherFragment", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled operation.
            }
        }
    }

    protected class Browser extends WebViewClient{
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

}
