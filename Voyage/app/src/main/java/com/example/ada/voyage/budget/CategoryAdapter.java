package com.example.ada.voyage.budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ada.voyage.R;

import java.util.ArrayList;

/**
 * Created by: Jiyoon Lim
 * Description: customized adapter: get category from category list and connect its attributes into view
 */

public class CategoryAdapter extends BaseAdapter {
    protected ArrayList<Category> categoryList;
    private Context context;

    public CategoryAdapter(Context context, ArrayList<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categoryList.get(position).hashCode();
    }

    public void add(Category category) {
        categoryList.add(category);
    }

    public void clear() {
        categoryList.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.calendar_listview_item, parent, false);

            CategoryViewHolder holder = new CategoryViewHolder();
            holder.categoryTitleTextView = (TextView)convertView.findViewById(R.id.list_event_title);
            convertView.setTag(holder);
        }

        Category category = categoryList.get(position);
        if (category != null) {
            CategoryViewHolder holder = (CategoryViewHolder) convertView.getTag();
            holder.categoryTitleTextView.setText(category.getTitle());
        }

        return convertView;
    }

    class CategoryViewHolder {
        protected TextView categoryTitleTextView;
    }
}
