package com.example.ada.voyage.blog;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ada.voyage.R;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.ada.voyage.blog.BlogFragment.bDBHelper;
import static com.example.ada.voyage.blog.BlogFragment.cardAdapter;
import static com.example.ada.voyage.blog.BlogFragment.cardList;
import static com.example.ada.voyage.blog.Hashtag.hashtag;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.ada.voyage.blog.BlogFragment.bDBHelper;
import static com.example.ada.voyage.blog.BlogFragment.cardAdapter;
import static com.example.ada.voyage.blog.BlogFragment.cardList;
import static com.example.ada.voyage.blog.Hashtag.hashtag;

/**
 * Created by: Jiwon Choi
 * Modified by: Jaebin Yang (Share post to Facebook)
 * Modified by: Jaebin Yang (Share & image display)
 * Description: The user can view the details of the post once he/she clicks a certain post from the overview list.
 */
public class ViewPostActivity extends AppCompatActivity {
    private Bitmap bitmap;
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    // attribute declarations
    private TextView viewTitle, viewDate, viewContent;
    private ImageView viewImage1, viewImage2, viewImage3, viewImage4, viewImage5;
    private BlogFragment bf = new BlogFragment();
    int position;
    ArrayList<SharePhoto> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
        photos = new ArrayList<SharePhoto>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog_activity_view_post);

        // Setup customized ActionBar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Details");
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Declaration of views in layout
        viewTitle = (TextView) findViewById(R.id.blog_view_title);
        viewDate = (TextView) findViewById(R.id.blog_view_date);
        viewImage1 = (ImageView) findViewById(R.id.blog_view_image_1);
        viewImage2 = (ImageView) findViewById(R.id.blog_view_image_2);
        viewImage3 = (ImageView) findViewById(R.id.blog_view_image_3);
        viewImage4 = (ImageView) findViewById(R.id.blog_view_image_4);
        viewImage5 = (ImageView) findViewById(R.id.blog_view_image_5);
        viewContent = (TextView) findViewById(R.id.blog_view_content);

        /**
         * Get each of the post(card) data from the previous screen.
         * In this case, we are getting the data from the "BlogFragment" class.
         * After getting those data, set those values on current screen so that user can view specific details of the post.
         */
        Intent intent = getIntent();
        viewTitle.setText(intent.getStringExtra("Post Title"));
        viewDate.setText(intent.getStringExtra("Post Date"));
        position = intent.getIntExtra("Post Position", -1);

        // Set up image to the ImageView
        if (cardList.get(position).getImageResource1().equals("Exception: No Image Applied")) {
            viewImage1.setVisibility(View.GONE);
        } else {
            Uri temp = Uri.parse(cardList.get(position).getImageResource1());
            Picasso.with(ViewPostActivity.this).load(temp).into(viewImage1);
        }

        if (cardList.get(position).getImageResource2().equals("Exception: No Image Applied")) {
            viewImage2.setVisibility(View.GONE);
        } else {
            Uri temp = Uri.parse(cardList.get(position).getImageResource2());
            Picasso.with(ViewPostActivity.this).load(temp).into(viewImage2);
        }

        if (cardList.get(position).getImageResource3().equals("Exception: No Image Applied")) {
            viewImage3.setVisibility(View.GONE);
        } else {
            Uri temp = Uri.parse(cardList.get(position).getImageResource3());
            Picasso.with(ViewPostActivity.this).load(temp).into(viewImage3);
        }

        if (cardList.get(position).getImageResource4().equals("Exception: No Image Applied")) {
            viewImage4.setVisibility(View.GONE);
        } else {
            Uri temp = Uri.parse(cardList.get(position).getImageResource4());
            Picasso.with(ViewPostActivity.this).load(temp).into(viewImage4);
        }

        if (cardList.get(position).getImageResource5().equals("Exception: No Image Applied")) {
            viewImage5.setVisibility(View.GONE);
        } else {
            Uri temp = Uri.parse(cardList.get(position).getImageResource5());
            Picasso.with(ViewPostActivity.this).load(temp).into(viewImage5);
        }

        /** Change hashtagged words' colors and allow users to click those words. */
        ArrayList<int[]> hashtagSpans = getSpans(intent.getStringExtra("Post Content"), '#');
        SpannableString spannableString = new SpannableString(intent.getStringExtra("Post Content"));

        for (int i = 0; i < hashtagSpans.size(); i++) {
            int[] span = hashtagSpans.get(i);
            int hashTagStart = span[0];
            int hashTagEnd = span[1];

            spannableString.setSpan(new Hashtag(getApplicationContext()), hashTagStart, hashTagEnd, 0);
        }
        viewContent.setMovementMethod(LinkMovementMethod.getInstance());
        viewContent.setText(spannableString);
        viewContent.setTextIsSelectable(true);

        viewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToHashtag = new Intent(ViewPostActivity.this, HashtagActivity.class);
                goToHashtag.putExtra("Hashtag", hashtag);
                startActivity(goToHashtag);
            }
        });
    }

    /**
     * Hashtag function
     *
     * @param body
     * @param prefix
     * @return ArrayList<int[]> spans
     */
    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<int[]>();

        Pattern pattern = Pattern.compile(prefix + "\\w+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    /**
     * Create customized option bar and set it up.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_blog_detail_bar, menu);
        return true;
    }

    /**
     * Customized menu bar with following buttons: "Share", "Edit", "Delete".
     * 1. "Edit" button: if the user clicks this button, the new Activity will be launched,
     * and he can edit the existing blog post.
     * 2. "Delete" button: if the user clicks this button, the existing blog post will be removed from the local database.
     * 3. "Share" button: if the user clicks this button, he can share the blog post on Facebook.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.blog_detail_bar_edit:
                Intent i = new Intent(ViewPostActivity.this, EditPostActivity.class);
                Intent pass = getIntent();

                i.putExtra("Edit Post Position", pass.getIntExtra("Post Position", -1));
                i.putExtra("Edit Post ID", pass.getIntExtra("Post ID", -1));

                finish();
                startActivity(i);

                break;

            case R.id.blog_detail_bar_delete:
                Intent intent = getIntent();
                int position = intent.getIntExtra("Post Position", -1);

                bDBHelper.deletePost(cardList.get(position));
//                cardList.remove(position);
                cardAdapter.remove(cardList.get(position));
                cardAdapter.notifyDataSetChanged();
                bf.onResume();

                finish();
                Toast.makeText(ViewPostActivity.this, "Deleted.", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When user clicks the "Back" button, go back to the previous screen.
     * In this case, the user will be navigated to the previous screen, to the class named "BlogFragment".
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    /**
     * Created by: Jaebin Yang
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Created by: Jaebin Yang
     *
     * @param item
     */
    public void onClick(MenuItem item) {
        /** When the user clicked to share to facebook */
        if (item.getItemId() == R.id.blog_detail_bar_share) {
            /** If the user clicks the settings icon, go to the settings activity after closing the drawer */
            if (item.getItemId() == R.id.blog_detail_bar_share) {
                Toast.makeText(ViewPostActivity.this, "Share to Facebook", Toast.LENGTH_SHORT).show();
                if (!cardList.get(position).getImageResource1().equals("Exception: No Image Applied")) {
                    Uri temp = Uri.parse(cardList.get(position).getImageResource1());
                    Picasso.with(ViewPostActivity.this)
                            .load(temp)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bm, Picasso.LoadedFrom from) {
                                    bitmap = bm;
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                    SharePhoto photo1 = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .setCaption(cardList.get(position).getCardContent())
                            .build();
                    photos.add(photo1);
                }
                if (!cardList.get(position).getImageResource2().equals("Exception: No Image Applied")) {
                    Uri temp = Uri.parse(cardList.get(position).getImageResource2());
                    Picasso.with(ViewPostActivity.this)
                            .load(temp)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bm, Picasso.LoadedFrom from) {
                                    bitmap = bm;
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                    SharePhoto photo2 = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .setUserGenerated(true)
                            .build();
                    photos.add(photo2);
                }
                if (!cardList.get(position).getImageResource3().equals("Exception: No Image Applied")) {
                    Uri temp = Uri.parse(cardList.get(position).getImageResource3());
                    Picasso.with(ViewPostActivity.this)
                            .load(temp)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bm, Picasso.LoadedFrom from) {
                                    bitmap = bm;
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                    SharePhoto photo3 = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .build();
                    photos.add(photo3);
                }
                if (!cardList.get(position).getImageResource4().equals("Exception: No Image Applied")) {
                    Uri temp = Uri.parse(cardList.get(position).getImageResource4());
                    Picasso.with(ViewPostActivity.this)
                            .load(temp)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bm, Picasso.LoadedFrom from) {
                                    bitmap = bm;
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                    SharePhoto photo4 = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .build();
                    photos.add(photo4);
                }
                if (!cardList.get(position).getImageResource5().equals("Exception: No Image Applied")) {
                    Uri temp = Uri.parse(cardList.get(position).getImageResource5());
                    Picasso.with(ViewPostActivity.this)
                            .load(temp)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bm, Picasso.LoadedFrom from) {
                                    bitmap = bm;
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                    SharePhoto photo5 = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .build();
                    photos.add(photo5);
                }

                if (ShareDialog.canShow(SharePhotoContent.class)) {
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .setPhotos(photos)
                            .setShareHashtag(new ShareHashtag.Builder()
                                    .setHashtag("#" + cardList.get(position).getCardContent()).build())
                            .build();

                    shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                }
                /** Facebook doesn't allow filling in texts when sharing (due to their change in policy a few years back);
                 * thus, we only share the photos stored in the blog post,
                 * and copy the blog description to the clipboard automatically
                 * so that the user only needs to paste the description before
                 * posting to Facebook
                 */
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("context", cardList.get(position).getCardContent());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(ViewPostActivity.this, "Blog content copied to clipboard.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
