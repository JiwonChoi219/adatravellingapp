package com.example.ada.voyage.calendar;

/**
 * Created by jiwonchoi on 2017. 5. 24..
 */

public class Schedule {
    private int id;
    private String title;
    private String location;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private String allDay;
    private String description;

    /**
     * Constructor with id
     *
     * @param id
     * @param title
     * @param location
     * @param startDate
     * @param startTime
     * @param endDate
     * @param endTime
     * @param allDay
     * @param description
     */
    public Schedule(int id, String title, String location, String startDate, String startTime,
                    String endDate, String endTime, String allDay, String description) {
        this.id = id;
        this.title = title;
        this.location = location;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endDate = endDate;
        this.endTime = endTime;
        this.allDay = allDay;
        this.description = description;
    }

    /**
     * Constructor without id
     *
     * @param title
     * @param location
     * @param startDate
     * @param startTime
     * @param endDate
     * @param endTime
     * @param allDay
     * @param description
     */
    public Schedule(String title, String location, String startDate, String startTime,
                    String endDate, String endTime, String allDay, String description) {
        this.title = title;
        this.location = location;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endDate = endDate;
        this.endTime = endTime;
        this.allDay = allDay;
        this.description = description;
    }

    /**
     * Accessor and mutator (getters and setters) for attributes
     */
    public int getId() {
        return id;
    }

    public void setId(int _id) {
        this.id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String  getAllDay() {
        return allDay;
    }

    public void setAllDay(String allDay) {
        this.allDay = allDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
