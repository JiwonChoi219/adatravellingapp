package com.example.ada.voyage.currency;

/*
 * Created by: YongHo Son
 * Modified by:
 * Description:
*/

import com.example.ada.voyage.R;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CurrencyFragment extends Fragment implements Callback<CurrencyExchange>, CurrencyItemClickListener{

    Context context;
    ListView lvCurrency;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_currency, null);
        lvCurrency = (ListView) view.findViewById(R.id.lvcurrency);
        loadCurrencyExchangeData();
        return view;
    }


    public boolean onSupportNavigateUp() {
        return onSupportNavigateUp();
    }

    /** Call Fixor.io api to get recent currency exchange rates */
    private void loadCurrencyExchangeData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CurrencyExchangeService service = retrofit.create(CurrencyExchangeService.class);
        Call<CurrencyExchange> call = service.loadCurrencyExchange();
        call.enqueue(this);
    }

    /** List view of countries adapts CurrencyAdapter's defined settings */
    @Override
    public void onResponse(Call<CurrencyExchange> call, Response<CurrencyExchange> response) {
        //Toast.makeText(this, response.body().getBase(), Toast.LENGTH_LONG).show();
        CurrencyExchange currencyExchange = response.body();
        lvCurrency.setAdapter(new CurrencyAdapter(getActivity(), currencyExchange.getCurrencyList(), this));
    }

    @Override
    public void onFailure(Call<CurrencyExchange> call, Throwable t) {
        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
    }

    /** Delivering the selected country's name, rate, flag information to CurrencyActivity_2 */
    @Override
    public void onCurrencyItemClick(Currency c, View view) {
        Intent intent = new Intent(getActivity(), CurrencyActivity_2.class);
        intent.putExtra("currency_inputName", c.getName1());
        intent.putExtra("currency_rate", c.getRate());
        intent.putExtra("flag_number", c.getFlag());
        startActivity(intent);

    }

}
