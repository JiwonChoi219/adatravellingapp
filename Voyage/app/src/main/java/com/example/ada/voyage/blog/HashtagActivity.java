package com.example.ada.voyage.blog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_CONTENT;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_DATE;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_ID_BLOG;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_1;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_2;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_3;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_4;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_IMG_5;
import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_TITLE_BLOG;
import static com.example.ada.voyage.blog.BlogFragment.bDBHelper;
import static com.example.ada.voyage.blog.BlogFragment.cardAdapter;
import static com.example.ada.voyage.blog.BlogFragment.cardList;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: The user can view the overview of posts that contains the searched hashtag.
 */

public class HashtagActivity extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Reuse the overview of the blog fragment layout.
        setContentView(R.layout.fragment_blog);

        // Setup the ActionBar title with a certain hashtag word that the user clicked to search for.
        Intent intent = getIntent();
        String actionBarTitle = intent.getStringExtra("Hashtag");

        // Setup customized ActionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("#" + actionBarTitle);
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Attribute declarations
        bDBHelper = new BlogDBHelper(this.getApplicationContext(), null, null, 1);
        listView = (ListView) findViewById(R.id.card_list);
        cardAdapter = new CardAdapter(getApplicationContext(), cardList);
        listView.setAdapter(cardAdapter);
    }

    /**
     * Get the list of blog posts from the recent to oldest.
     * In this case, only get the blog post which contains a certain hashtagged word in contents.
     * The user cannot view the details of the post; he can only see the overview of the post.
     */
    @Override
    public void onResume() {
        super.onResume();

        Intent intent = getIntent();
        String search = intent.getStringExtra("Hashtag");

        cardAdapter.clear();
        try {
            JSONArray array = bDBHelper.viewHashtag("#" + search);
            for (int i = array.length() - 1; i >= 0; i--) {
                JSONObject obj = (JSONObject) array.get(i);
                cardAdapter.add(new Card(obj.getInt(COLUMN_ID_BLOG),
                        obj.get(COLUMN_TITLE_BLOG).toString(),
                        obj.get(COLUMN_DATE).toString(),
                        obj.get(COLUMN_IMG_1).toString(),
                        obj.get(COLUMN_IMG_2).toString(),
                        obj.get(COLUMN_IMG_3).toString(),
                        obj.get(COLUMN_IMG_4).toString(),
                        obj.get(COLUMN_IMG_5).toString(),
                        obj.get(COLUMN_CONTENT).toString()
                ));
            }
            cardAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Failed to load database", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * When user clicks the "Back" button, the user will move back to the previous screen.
     * In this case, the user will move to the Activity screen, to the class named "ViewPostActivity".
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
