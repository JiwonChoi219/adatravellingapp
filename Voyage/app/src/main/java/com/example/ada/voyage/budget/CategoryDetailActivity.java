package com.example.ada.voyage.budget;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_AMOUNT;
import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_CATEGORY_ID;
import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_DATE;
import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_ID;
import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_SIGN;
import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_TITLE;
import static com.example.ada.voyage.budget.MoneyDBHelper.COLUMN_MONEY_TRIP_ID;
import static com.example.ada.voyage.budget.TripDetailActivity.categoryDBHelper;
import static com.example.ada.voyage.budget.TripDetailActivity.categoryList;

/**
 * Created by: Jiyoon Lim
 * Modified by: Jiwon Choi (DB)
 * Description: Displays the list of moneys that the user added.
 * The user can add, edit or remove the money
 * A total money spent and left are calculated
 */

public class CategoryDetailActivity extends AppCompatActivity {

    //listView for money list
    protected static ListView listView;
    protected static MoneyAdapter mAdapter;
    protected static MoneyDBHelper moneyDBHelper;
    protected static ArrayList<Money> moneyList = new ArrayList<>();
    private TextView categoryTitleTextView, categoryLeftTextView, categorySpentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_detail);

        Intent intent = getIntent();
        int position = intent.getIntExtra("category detail position", -1);
        final Category category = categoryList.get(position);

        categoryTitleTextView = (TextView) findViewById(R.id.budget_title);
        categoryLeftTextView = (TextView) findViewById(R.id.budget_total_money_left);
        categorySpentTextView = (TextView) findViewById(R.id.budget_total_money_spent);

        mAdapter = new MoneyAdapter(CategoryDetailActivity.this, moneyList);
        moneyDBHelper = new MoneyDBHelper(CategoryDetailActivity.this, null, null, 1);

        listView = (ListView) findViewById(R.id.budget_list);
        listView.setAdapter(mAdapter);

        categoryTitleTextView.setText(category.getTitle());
        categoryLeftTextView.setText(category.getMoneyLeft());
        categorySpentTextView.setText(category.getMoneySpent());

        //money list
        //when clicking a certain money, it goes view money detail
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, final int position, long id) {
                Money money = moneyList.get(position);

                Intent intent = new Intent(CategoryDetailActivity.this, ViewMoneyDetailActivity.class);

                intent.putExtra("money detail id", money.getId());
                intent.putExtra("money detail position", position);
                startActivity(intent);
            }
        });
        //end of clicking an item

        //actionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Category Details");
        actionBar.setDisplayHomeAsUpEnabled(true);
        //end of actionBar

        //When clicking a floating button to add a new money, display money detail with no information
        FloatingActionButton addMoneyButton;
        addMoneyButton = (FloatingActionButton) findViewById(R.id.budget_add_btn);
        addMoneyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoryDetailActivity.this, AddMoneyDetailActivity.class);

                Intent pass = getIntent();
                intent.putExtra("trip detail id", pass.getIntExtra("trip detail id", -1));
                intent.putExtra("category detail id", category.getId());
                startActivity(intent);
            }
        }); //end of floating button

    } // end onCreate()

    //calutae a total money
    protected String calculateSpent(){
        int total = 0;
        for(int i = 0; i < mAdapter.getCount(); i++){
            Money m = (Money) mAdapter.getItem(i);
            if(m.getSign().equals("-")){
                int a = Integer.parseInt(m.getAmount());
                total += a;
            }
        }
        return String.valueOf(total);
    }
    //calutae a total money
    protected String calculateLeft(){
        int total = 0;
        String totalStr = "";
        for(int i = 0; i < mAdapter.getCount(); i++){
            Money m = (Money) mAdapter.getItem(i);
            if(m.getSign().equals("-")){
                int a = Integer.parseInt(m.getAmount());
                total -= a;
            } else{//m.getSign().equals("+")
                int a = Integer.parseInt(m.getAmount());
                total += a;
            }
        }
        totalStr = String.valueOf(total);
        return totalStr;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //get data from database and view them on the list
        Intent intent = getIntent();
        int id = intent.getIntExtra("category detail id", -1);

        mAdapter.clear();
        try {
            JSONArray array = moneyDBHelper.viewMoney(id);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                mAdapter.add(new Money(obj.getInt(COLUMN_MONEY_ID),
                        obj.getInt(COLUMN_MONEY_TRIP_ID),
                        obj.getInt(COLUMN_MONEY_CATEGORY_ID),
                        obj.get(COLUMN_MONEY_TITLE).toString(),
                        obj.get(COLUMN_MONEY_DATE).toString(),
                        obj.get(COLUMN_MONEY_AMOUNT).toString(),
                        obj.get(COLUMN_MONEY_SIGN).toString()
                ));
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Toast.makeText(this.getApplicationContext(), "Failed to load database", Toast.LENGTH_LONG).show();
        }

        int position = intent.getIntExtra("category detail position", -1);

        categoryList.get(position).setMoneyLeft(calculateLeft());
        categoryList.get(position).setMoneySpent(calculateSpent());
        categoryDBHelper.editCategory(categoryList.get(position));

        categoryLeftTextView.setText( categoryList.get(position).getMoneyLeft());
        categorySpentTextView.setText( categoryList.get(position).getMoneySpent());
    }

    //back button
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    } //end of <-
}
