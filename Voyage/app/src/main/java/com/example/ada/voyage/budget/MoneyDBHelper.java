package com.example.ada.voyage.budget;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by: Jiwon Choi
 * Modified by: Jiyoon Lim
 * Description: database of moneys with its attributes.
 */

public class MoneyDBHelper extends SQLiteOpenHelper {
    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database information
    private static final String DATABASE_NAME = "MoneyDB.db";

    // Table name
    private static final String TABLE_NAME = "MoneyTable";

    // Table columns
    public static final String COLUMN_MONEY_ID = "_id";
    public static final String COLUMN_MONEY_TRIP_ID = "mtid";
    public static final String COLUMN_MONEY_CATEGORY_ID = "mcid";
    public static final String COLUMN_MONEY_TITLE = "moneyTitle";
    public static final String COLUMN_MONEY_DATE = "moneyDate";
    public static final String COLUMN_MONEY_AMOUNT = "moneyAmount";
    public static final String COLUMN_MONEY_SIGN = "moneySign";

    private static final String query = "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_MONEY_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_MONEY_TRIP_ID + " TEXT, " + COLUMN_MONEY_CATEGORY_ID +
            " TEXT, " + COLUMN_MONEY_TITLE + " TEXT, " + COLUMN_MONEY_DATE + " TEXT, " + COLUMN_MONEY_AMOUNT +
            " TEXT, " + COLUMN_MONEY_SIGN +  " TEXT );";

    public MoneyDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addMoney(Money money) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_MONEY_TRIP_ID, money.getTripId());
        values.put(COLUMN_MONEY_CATEGORY_ID, money.getCategoryId());
        values.put(COLUMN_MONEY_TITLE, money.getTitle());
        values.put(COLUMN_MONEY_DATE, money.getDate());
        values.put(COLUMN_MONEY_AMOUNT, money.getAmount());
        values.put(COLUMN_MONEY_SIGN, money.getSign());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void editMoney(Money money) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_MONEY_TITLE, money.getTitle());
        values.put(COLUMN_MONEY_DATE, money.getDate());
        values.put(COLUMN_MONEY_AMOUNT, money.getAmount());
        values.put(COLUMN_MONEY_SIGN, money.getSign());
        db.update(TABLE_NAME, values, COLUMN_MONEY_ID + "=" + money.getId(), null);
        db.close();
    }

    public JSONArray viewMoney(int categoryId) throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE (" + COLUMN_MONEY_CATEGORY_ID + " = " + String.valueOf(categoryId) + ");";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_MONEY_ID, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_MONEY_TRIP_ID, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_TRIP_ID)));
            object.put(COLUMN_MONEY_CATEGORY_ID, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_CATEGORY_ID)));
            object.put(COLUMN_MONEY_TITLE, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_TITLE)));
            object.put(COLUMN_MONEY_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_DATE)));
            object.put(COLUMN_MONEY_AMOUNT, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_AMOUNT)));
            object.put(COLUMN_MONEY_SIGN, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_SIGN)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    public void deleteMoney(Money money) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_MONEY_ID + " = " + money.getId() + ");";
        db.execSQL(query);
        db.close();
    }

    /** Jaebin Yang:
     * Delete all the money objects in the database.
     */

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME;
        db.execSQL(query);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME + "'");
        db.close();
    }

    /** Jaebin Yang:
     * return all the money objecst in the database.
     */
    public JSONArray money() throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME ;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_MONEY_ID, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_MONEY_TRIP_ID, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_TRIP_ID)));
            object.put(COLUMN_MONEY_CATEGORY_ID, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_CATEGORY_ID)));
            object.put(COLUMN_MONEY_TITLE, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_TITLE)));
            object.put(COLUMN_MONEY_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_DATE)));
            object.put(COLUMN_MONEY_AMOUNT, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_AMOUNT)));
            object.put(COLUMN_MONEY_SIGN, cursor.getString(cursor.getColumnIndex(COLUMN_MONEY_SIGN)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    /** Jaebin Yang
     * When a Trip is deleted, delete the Money saved in the trip.
     * @param tripID
     */
    public void deleteMoneyFromTrip(int tripID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_MONEY_TRIP_ID + " = " + tripID + ");";
        db.execSQL(query);
        db.close();
    }

    /** Jaebin Yang
     * When a Category is deleted, delte the Money saved in the Category */
    public void deleteMoneyFromCategory(int categoryID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_MONEY_CATEGORY_ID+ " = " + categoryID + ");";
        db.execSQL(query);
        db.close();
    }


}
