package com.example.ada.voyage.budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ada.voyage.R;

import java.util.ArrayList;

/**
 * Created by: Jiyoon Lim
 * Description: customized adapter: get trip from trip list and connect its attributes into view
 */

public class TripAdapter extends BaseAdapter {
    protected ArrayList<Trip> tripList;
    private Context context;

    public TripAdapter(Context context, ArrayList<Trip> tripList) {
        this.context = context;
        this.tripList = tripList;
    }

    @Override
    public int getCount() {
        return tripList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    public void add(Trip trip) {
        tripList.add(trip);
    }

    public void clear() {
        tripList.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.calendar_listview_item, parent, false);

            TripViewHolder holder = new TripViewHolder();
            holder.tripTitleTextView = (TextView)convertView.findViewById(R.id.list_event_title);
            convertView.setTag(holder);
        }

        Trip trip = tripList.get(position);
        if (trip != null) {
            TripViewHolder holder = (TripViewHolder) convertView.getTag();
            holder.tripTitleTextView.setText(trip.getTitle());
        }

        return convertView;
    }

    class TripViewHolder {
        protected TextView tripTitleTextView;
    }
}
