package com.example.ada.voyage.budget;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by: Jiwon Choi
 * Modified by: Jiyoon Lim
 * Description: database of categories with its attributes.
 */

public class CategoryDBHelper extends SQLiteOpenHelper {
    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database information
    private static final String DATABASE_NAME = "CategoryDB.db";

    // Table name
    private static final String TABLE_NAME = "CategoryTable";

    // Table columns
    public static final String COLUMN_CATEGORY_ID = "_id";
    public static final String COLUMN_CATEGORY_TRIP_ID = "tid";
    public static final String COLUMN_CATEGORY_TITLE = "categoryTitle";
    public static final String COLUMN_CATEGORY_SPENT = "categorySpent";
    public static final String COLUMN_CATEGORY_LEFT = "categoryLeft";

    private static final String query = "CREATE TABLE " + TABLE_NAME + "(" +
            COLUMN_CATEGORY_ID + " TEXT, " + COLUMN_CATEGORY_TRIP_ID + " TEXT, " +
            COLUMN_CATEGORY_TITLE + " TEXT, " + COLUMN_CATEGORY_SPENT + " TEXT, " + COLUMN_CATEGORY_LEFT + " TEXT );";

    public CategoryDBHelper (Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CATEGORY_ID, category.getId());
        values.put(COLUMN_CATEGORY_TRIP_ID, category.getTripId());
        values.put(COLUMN_CATEGORY_TITLE, category.getTitle());
        values.put(COLUMN_CATEGORY_SPENT, category.getMoneySpent());
        values.put(COLUMN_CATEGORY_LEFT, category.getMoneyLeft());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void editCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_CATEGORY_TITLE, category.getTitle());
        values.put(COLUMN_CATEGORY_SPENT, category.getMoneySpent());
        values.put(COLUMN_CATEGORY_LEFT, category.getMoneyLeft());

        db.update(TABLE_NAME, values, COLUMN_CATEGORY_ID + "=" + category.getId(), null);
        db.close();
    }

    /**
     * @return JSONArray array
     * @throws JSONException
     */
    public JSONArray viewCategory(int tripID) throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE (" + COLUMN_CATEGORY_TRIP_ID + " = " + tripID + ");";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_CATEGORY_ID, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_CATEGORY_TRIP_ID, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_TRIP_ID)));
            object.put(COLUMN_CATEGORY_TITLE, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_TITLE)));
            object.put(COLUMN_CATEGORY_SPENT, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_SPENT)));
            object.put(COLUMN_CATEGORY_LEFT, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_LEFT)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    public void deleteCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_CATEGORY_ID + " = " + category.getId() + ");";
        db.execSQL(query);
        db.close();
    }

    /** Jaebin Yang:
     * When a Trip is deleted, delete the Catetories saved in the Trip as well
     * @param tripID
     */
    public void deleteCategoryFromTrip(int tripID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_CATEGORY_TRIP_ID + " = " + tripID + ");";
        db.execSQL(query);
        db.close();
    }


    /** Jaebin Yang:
     * Delete all the category objects in the database.
     */
    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME;
        db.execSQL(query);
        db.close();
    }

    /** Return all the category objects stored in the database. */
    public JSONArray categories() throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_CATEGORY_ID, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_CATEGORY_TRIP_ID, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_TRIP_ID)));
            object.put(COLUMN_CATEGORY_TITLE, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_TITLE)));
            object.put(COLUMN_CATEGORY_SPENT, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_SPENT)));
            object.put(COLUMN_CATEGORY_LEFT, cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_LEFT)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

}
