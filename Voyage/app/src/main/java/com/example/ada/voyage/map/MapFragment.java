package com.example.ada.voyage.map;

/*
 * Created by: YongHo Son
 * Modified by:
 * Description:
*/

import com.example.ada.voyage.R;

import android.Manifest;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Text;

import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    MapView map;
    private GoogleMap googleMap;
    private GPSTracker gpsTracker;
    private Location mLocation;
    double latitude, longitude;


    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final String TAG = "MapsFragment";

    private ZoomControls zoom;
    private Button geoLocationBT;
    private Button navigationBT;
    private EditText etLocationEntry;
    private TextView threeAddress;
    private String tempName = "";

    //What3words attributes
    String apiKey = "DK3JI1UT";
    private String baseUrl = "https://api.what3words.com/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        /** Check whether the user gave permission to access location or not before start functionality */
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return loadActivity(inflater, container, savedInstanceState);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
            return null;
        }
    }

    /** When the app has a permission to access the location */
    private View loadActivity(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        map = (MapView) view.findViewById(R.id.frag_map);
        map.onCreate(savedInstanceState);
        map.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /** GPS */
        gpsTracker = new GPSTracker(getActivity());
        mLocation = gpsTracker.getLocation();
        latitude = mLocation.getLatitude();
        longitude = mLocation.getLongitude();
        threeAddress = (TextView) view.findViewById(R.id.wordaddress);

        /** What3Words (Coordinates to three word address) */
        String[] coordinateStrings = {String.valueOf(latitude), String.valueOf(latitude)};
        String coordinateString = coordinateStrings[0]+ "," +coordinateStrings[1];

        /** To avoid runtime error */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String outputURL = new  What3WordsRequest(baseUrl, apiKey).RequestW3W(coordinateString);

        /** Extracting three word address section to display from HTTP call */
        boolean controlPrint = true;
        for(int i=0; i<outputURL.length(); i++) {
            if (outputURL.charAt(i) == '[') {
                for (int j = i; j < outputURL.length(); j++) {
                    if (controlPrint) {
                        threeAddress.setText(outputURL.substring(i, j+1));
                        if (outputURL.charAt(j) == ']') {
                            controlPrint = false;
                        }
                    }
                }
            }
        }


        /** Displaying Google Map */
        map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For dropping a marker at a point on the Map
                LatLng current = new LatLng(latitude, longitude);
                googleMap.addMarker(new MarkerOptions().position(current).title("Marker Title").snippet("Marker Description"));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(5).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
                    }
                }

            }
        });

        /** Zoom In & Out Function */
        zoom = (ZoomControls) view.findViewById(R.id.frag_zcZoom);

        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });
        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });

        geoLocationBT = (Button) view.findViewById(R.id.frag_btSearch);
        etLocationEntry = (EditText) view.findViewById(R.id.frag_etLocationEntry);

        geoLocationBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleMap.clear();
                tempName = etLocationEntry.getText().toString();
                String search = tempName;
                boolean control3= true;

                /** When the input is three word address */
                for(int a=0; a<search.length(); a++){
                    if(control3) {
                        if (search.charAt(a) == '.') {
                            control3 = false;

                            /** What3Words (Receive lat, long) */
                            String coordinateURL = new What3WordsRequest(baseUrl, apiKey).RequestPosition(search);

                            boolean control1 = true;
                            boolean control2 = true;
                            String addressTemp = "";
                            String latTemp = "";
                            String lonTemp= "";

                            for (int i = 0; i < coordinateURL.length(); i++) {
                                if (coordinateURL.charAt(i) == '[') {
                                    for (int j = i; j < coordinateURL.length(); j++) {
                                        addressTemp = coordinateURL.substring(i,j);
                                    }

                                }
                            }

                            /** Extracting only latitude section from HTTP call */
                            for (int k=0; k< addressTemp.length(); k++){
                                if(addressTemp.charAt(k) == '['){
                                    for (int t=k; t<addressTemp.length(); t++){
                                        if (control1) {
                                            if (addressTemp.charAt(t) == ','){
                                                latTemp = addressTemp.substring(1,t);
                                                control1 = false;
                                            }
                                        }

                                    }
                                    /** Extracting only longitude section from HTTP call */
                                    for (int c=0; c<addressTemp.length(); c++){
                                        if (addressTemp.charAt(c) == ','){
                                            for (int d=c; d<addressTemp.length(); d++){
                                                if (control2){
                                                    if (addressTemp.charAt(d) == ']'){
                                                        control2 = false;
                                                        lonTemp = addressTemp.substring(c+1, d);
                                                    }
                                                }
                                            }
                                        }
                                    }



                                }
                            }

                            /** Convert String value of lat and lon into double */
                            double w3wLat1 = Double.parseDouble(latTemp);
                            double w3wLong1 = Double.parseDouble(lonTemp);

                            LatLng latLng = new LatLng(w3wLat1, w3wLong1);
                            googleMap.addMarker(new MarkerOptions().position(latLng).title(""));
                            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                            /** What3Words (Receive 3 word Address for displaying) */
                            String coordinateString = latTemp + "," + lonTemp;
                            String outputURL = new What3WordsRequest(baseUrl, apiKey).RequestW3W(coordinateString);

                            boolean controlPrint = true;
                            for (int i = 0; i < outputURL.length(); i++) {
                                if (outputURL.charAt(i) == '[') {
                                    for (int j = i; j < outputURL.length(); j++) {
                                        if (controlPrint) {
                                            threeAddress.setText(outputURL.substring(i, j + 1));
                                            if (outputURL.charAt(j) == ']') {
                                                controlPrint = false;
                                            }
                                        }
                                    }
                                }
                            }



                        }
                    }
                }

                /** Case when place name is not three word address */
                if (control3){
                    if (search != null && !search.equals("")) {
                        List<Address> addressList = null;
                        Geocoder geocoder = new Geocoder(getActivity());
                        try {
                            addressList = geocoder.getFromLocationName(search, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Address address = addressList.get(0);
                        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        googleMap.addMarker(new MarkerOptions().position(latLng).title(""));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                        /** What3Words (Receive 3 word Address for displaying) */
                        String[] coordinateStrings = {String.valueOf(address.getLatitude()), String.valueOf(address.getLongitude())};
                        String coordinateString = coordinateStrings[0] + "," + coordinateStrings[1];
                        String outputURL = new What3WordsRequest(baseUrl, apiKey).RequestW3W(coordinateString);

                        /** Extracting only three word address section from HTTP call */
                        boolean controlPrint = true;
                        for (int i = 0; i < outputURL.length(); i++) {
                            if (outputURL.charAt(i) == '[') {
                                for (int j = i; j < outputURL.length(); j++) {
                                    if (controlPrint) {
                                        threeAddress.setText(outputURL.substring(i, j + 1));
                                        if (outputURL.charAt(j) == ']') {
                                            controlPrint = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        });

        /** Go to Navigation Activity*/
        navigationBT = (Button) view.findViewById(R.id.frag_navigationBT);
        navigationBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NavigationActivity.class);
                startActivity(i);
            }
        });

        return view;
    }


    /** Add a marker to current location and move the camera */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng sydney = new LatLng(latitude, longitude);
        googleMap.addMarker(new MarkerOptions().position(sydney).title("Current Location"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
    }

    /** Dealing with the case when the users whether allows location access of the map functionality or not */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(this).attach(this).commitAllowingStateLoss();
                    }
                } else {
                    Toast.makeText(getActivity(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}


