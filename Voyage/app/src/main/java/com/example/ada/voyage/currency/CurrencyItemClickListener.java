package com.example.ada.voyage.currency;

import android.view.View;

/**
 * Created by YongHo on 2017. 5. 23..
 */

public interface CurrencyItemClickListener {
    void onCurrencyItemClick(Currency c, View view);
}
