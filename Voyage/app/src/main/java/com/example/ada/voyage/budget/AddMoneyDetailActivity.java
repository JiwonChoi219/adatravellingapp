package com.example.ada.voyage.budget;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.ada.voyage.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.example.ada.voyage.budget.CategoryDetailActivity.moneyDBHelper;
import static com.example.ada.voyage.budget.CategoryDetailActivity.moneyList;
import static com.example.ada.voyage.budget.CategoryDetailActivity.mAdapter;

/**
 * Created by: Jiyoon Lim
 * Modified by: Jiwon Choi (data picker dialog)
 * Description: add money according to entered information.
 */

public class AddMoneyDetailActivity extends AppCompatActivity {
    EditText dateEditText;
    int year, month, day;
    EditText amountEditText;
    EditText titleEditText;
    RadioGroup amountTypeRadioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_detail);

        //action bar shown on the top of activity
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Add a new money");
        actionBar.setDisplayHomeAsUpEnabled(true);

        dateEditText = (EditText) findViewById(R.id.money_detail_date);
        titleEditText = (EditText) findViewById(R.id.money_detail_title);
        amountEditText = (EditText) findViewById(R.id.money_detail_amount);
        amountTypeRadioButton = (RadioGroup) findViewById(R.id.money_detail_radiogroup);

        GregorianCalendar calendar = new GregorianCalendar();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddMoneyDetailActivity.this, dateSetListener, year, month, day).show();
            }
        });

    } // end onCreate()

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bar_cancel:
                finish();
                break;

            //when clicking a save button, add money with entered information (titla, date, amount,)
            case R.id.action_bar_save:

                String dateStr = dateEditText.getText().toString();
                String amountStr = amountEditText.getText().toString();
                String titleStr = titleEditText.getText().toString();
                String signStr;
                int checkedRadioID = amountTypeRadioButton.getCheckedRadioButtonId();

                Intent intent = getIntent();
                int categoryID = intent.getIntExtra("category detail id", -1);

                if(dateStr.length()==0||amountStr.length()==0||titleStr.length()==0){
                    Toast.makeText(AddMoneyDetailActivity.this, "Fill out the all blanks first", Toast.LENGTH_SHORT).show();
                }
                else{
                    Money money;
                    if(checkedRadioID==R.id.money_detail_negative_radiobutton){
                        signStr = "-";
                        money = new Money(titleStr, dateStr, amountStr, signStr);
                        money.setCategoryId(categoryID);
                        money.setAmount(amountStr);
                        money.setSign(signStr);
                        moneyDBHelper.addMoney(money);
                        moneyList.add(money);
                        mAdapter.add(money);
                        mAdapter.notifyDataSetChanged();
                        finish();
                    }
                    else if(checkedRadioID==R.id.money_detail_postive_radiobutton){
                        signStr = "+";
                        money = new Money(titleStr, dateStr, amountStr, signStr);
                        money.setCategoryId(categoryID);
                        moneyDBHelper.addMoney(money);
                        moneyList.add(money);
                        mAdapter.add(money);
                        mAdapter.notifyDataSetChanged();
                        finish();
                    }
                    else{
                        Toast.makeText(AddMoneyDetailActivity.this, "Check a type of amount first", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // back icon
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    // Date picker dialog (Jiwon choi)
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg = String.format("%d/%d/%d", monthOfYear + 1, dayOfMonth, year);

            if (monthOfYear < 10 || dayOfMonth < 10) {
                String month = "", day = "";

                if (monthOfYear < 10) {
                    month = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    month = String.valueOf(monthOfYear + 1);
                }

                if (dayOfMonth < 10) {
                    day = "0" + String.valueOf(dayOfMonth);
                } else {
                    day = String.valueOf(dayOfMonth);
                }

                msg = String.format("%s/%s/%d", month, day, year);
            }

            dateEditText.setText(msg);
        }
    };
}
