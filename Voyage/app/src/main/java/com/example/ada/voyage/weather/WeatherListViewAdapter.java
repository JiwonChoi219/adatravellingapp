package com.example.ada.voyage.weather;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ada.voyage.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.ada.voyage.weather.WeatherFragment.mAdapter;

/**
 * Created by JiyoonLim
 * customized adapter for composing weather list
 */

public class WeatherListViewAdapter extends BaseAdapter{

    private Context context;
    /*
    * array list : consists weather items which is added in adapter*/
    private ArrayList<WeatherListViewItem> weatherArrayList;

    /*for getting and setting weather icons*/
    Map<String, Bitmap> bitmaps = new HashMap<>();

    //constructor
    public WeatherListViewAdapter(Context context, ArrayList<WeatherListViewItem> weatherArrayList) {
        this.context = context;
        this.weatherArrayList = weatherArrayList;
    }

    //overrides custom methos
    @Override
    public int getCount() {
        return weatherArrayList.size();
    }

    @Override
    public WeatherListViewItem getItem(int position) {
        return weatherArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //for custom listview which contains day description and temperatures.
    private static class ViewHolder {
        protected TextView dayTV;
        protected TextView descriptionTV;
        protected TextView maxTempTV;
        protected TextView minTempTV;
        protected ImageView icomIV;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        /*if there is not existing holder, make one*/
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.weather_list_item, parent, false);
            holder.dayTV = (TextView) convertView.findViewById(R.id.weather_day);
            holder.descriptionTV = (TextView) convertView.findViewById(R.id.weather_description);
            holder.maxTempTV = (TextView) convertView.findViewById(R.id.weather_maxTemp);
            holder.minTempTV = (TextView) convertView.findViewById(R.id.weather_minTemp);
            holder.icomIV = (ImageView) convertView.findViewById(R.id.weather_icon);
            convertView.setTag(holder);
        }else{
            /*if there is existing holder, reuse it*/
            holder = (ViewHolder) convertView.getTag();
        }

        // get item with a certain position from weatherlist
        WeatherListViewItem weather = weatherArrayList.get(position);
        /*set attributes of weather item into view*/
        holder.dayTV.setText(weather.dayStr);
        holder.descriptionTV.setText(weather.descriptionStr);
        holder.minTempTV.setText(weather.minTempStr);
        holder.maxTempTV.setText(weather.maxTempStr);


        /* if weather icon is already downloaded, reuse it*/
        if (bitmaps.containsKey(weather.iconStr)) {
            holder.icomIV.setImageBitmap(
                    bitmaps.get(weather.iconStr));
        }
        else {
            /*download icon and set icon into view*/
            new LoadImageTask(holder.icomIV).execute(weather.iconStr);
        }
        return convertView;
    }

    private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView imageView;
        public LoadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        /*
        * open an HttpURLConnection and download the icon
        * then close the HttpURLConnection*/
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();

                try (InputStream inputStream = connection.getInputStream()) {
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    bitmaps.put(params[0], bitmap); // cache for later use
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                connection.disconnect();
            }
            return bitmap;
        }

        // set weather icon in list item
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }

    public void add(WeatherListViewItem weatherListViewItem){
        weatherArrayList.add(weatherListViewItem);
    }

    public void clear() {
        weatherArrayList.clear();
        mAdapter.notifyDataSetChanged();
    }

}
