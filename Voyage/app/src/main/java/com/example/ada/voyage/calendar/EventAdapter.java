package com.example.ada.voyage.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ada.voyage.R;

import java.util.ArrayList;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description:
 */

public class EventAdapter extends BaseAdapter {
    protected ArrayList<Schedule> scheduleList;
    private Context context;

    public EventAdapter(Context context, ArrayList<Schedule> list) {
        this.context = context;
        this.scheduleList = list;
    }

    @Override
    public int getCount() {
        return scheduleList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    public void add(Schedule e) {
        scheduleList.add(e);
    }

    public void clear() {
        scheduleList.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.calendar_listview_item, parent, false);

            EventViewHolder holder = new EventViewHolder();
            holder.titleTextView = (TextView)convertView.findViewById(R.id.list_event_title);
            convertView.setTag(holder);
        }

        Schedule schedule = scheduleList.get(position);
        if (schedule != null) {
            EventViewHolder holder = (EventViewHolder)convertView.getTag();
            holder.titleTextView.setText(schedule.getTitle());
        }

        return convertView;
    }

    class EventViewHolder {
        protected TextView titleTextView;
    }

}

