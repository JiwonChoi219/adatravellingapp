package com.example.ada.voyage.blog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ada.voyage.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.ada.voyage.blog.BlogFragment.bDBHelper;
import static com.example.ada.voyage.blog.BlogFragment.cardAdapter;

/**
 * Created by: Jiwon Choi
 * Modified by: Jaebin Yang (Saving Image)
 * Modified by: Jaebin Yang
 * Description: The user can add a new post with this activity.
 */

public class AddPostActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    FirebaseUser currentUser;
    String personId;
    StorageReference storageRef;
    String newResource;
    private static final int POST_CAMERA = 0;
    private static final int POST_ALBUM = 1;
    private static final int POST_CROP = 2;

    private String imageUri1, imageUri2, imageUri3, imageUri4, imageUri5;
    private Uri cameraUri, albumUri = null;
    private TextView title, content;
    private Long date;
    private ImageView image1, image2, image3, image4, image5;
    private FloatingActionButton addImageButton;
    private String currentPath;
    private boolean album = false;

    private int selectedGlobal = -1;

    private BlogFragment bf = new BlogFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        personId = (String) currentUser.getUid();
        storageRef = FirebaseStorage.getInstance().getReference();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog_activity_post);

        /**
         * ActionBar customize
         */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Add Post");
        actionBar.setDisplayHomeAsUpEnabled(true);

        /**
         * attribute declarations
         */
        title = (TextView) this.findViewById(R.id.post_title);
        date = System.currentTimeMillis();
        content = (TextView) this.findViewById(R.id.post_content);
        image1 = (ImageView) this.findViewById(R.id.post_image_1);
        image2 = (ImageView) this.findViewById(R.id.post_image_2);
        image3 = (ImageView) this.findViewById(R.id.post_image_3);
        image4 = (ImageView) this.findViewById(R.id.post_image_4);
        image5 = (ImageView) this.findViewById(R.id.post_image_5);
        addImageButton = (FloatingActionButton) this.findViewById(R.id.post_add_image);

        /**
         * The spaces for images are currently invisible;
         * once the user adds new images, it will be appeared.
         */
        image1.setVisibility(View.GONE);
        image2.setVisibility(View.GONE);
        image3.setVisibility(View.GONE);
        image4.setVisibility(View.GONE);
        image5.setVisibility(View.GONE);

        image1.setDrawingCacheEnabled(true);
        image2.setDrawingCacheEnabled(true);
        image3.setDrawingCacheEnabled(true);
        image4.setDrawingCacheEnabled(true);
        image5.setDrawingCacheEnabled(true);

        /**
         * When the user clicks 'Add Image' button, the dialog below will be pop up.
         * This allows users to select image from the gallery or to take a new photo.
         */
        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder uploadImage = new AlertDialog.Builder(AddPostActivity.this);
                uploadImage.setTitle("Upload Image");

                uploadImage.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCamera();
                        dialog.dismiss();
                    }
                });

                uploadImage.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAlbum();
                        dialog.dismiss();
                    }
                });

                uploadImage.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                uploadImage.show();
            }
        });

    }

    /**
     * Depending on the command that the user would like to do,
     * the switch case will sort the command and let user to do so.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case POST_CAMERA:
                cropImage();
                break;
            case POST_ALBUM:
                album = true;
                File albumFile = null;
                try {
                    albumFile = createImageFile();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                if (albumFile != null) {
                    albumUri = Uri.fromFile(albumFile);
                }
                if (data != null) {
                    cameraUri = data.getData();
                }
                cropImage();
                break;

            case POST_CROP:
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                if (album == false) {
                    intent.setData(cameraUri);
                } else if (album == true) {
                    album = false;
                    intent.setData(albumUri);
                }
                this.sendBroadcast(intent);
                setImage();
                break;
        }
    }

    /**
     * Customized menu bar with following buttons: "Share", "Edit", "Delete".
     * 1. "Cancel" button: if the user clicks this button, the user will move back to the previous Activity/screen.
     * 2. "Save" button: if the user clicks this button, the information filled in will be saved to the local database.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bar_cancel:
                finish();
                break;
            case R.id.action_bar_save:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setMessage("Saving Post...");
                long now = System.currentTimeMillis();
                Date date = new Date(now);
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                String today = format.format(date);

                if (title.getText().length() == 0 || content.getText().length() == 0) {
                    Toast.makeText(AddPostActivity.this, "Invalid input: Check your input field.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddPostActivity.this,
                            "Saving Post...", Toast.LENGTH_LONG).show();

                    /** Jaebin Yang:
                     * If the device is connected to the internet add the images to the Firebase Storage,
                     * and retrieve the new uri as given in the Firebase Storage
                     * and save this uri to the local database.
                     * so that it is possible to sync this uri on another device.
                     * Otherwise, the images are saved using the local image path (media)
                     * And this image is uploaded to the FIrebase Storage when the users upload the data to Firebase.
                     */

                    if (imageUri1 == null) {
                        imageUri1 = "Exception: No Image Applied";
                    }
                    if (imageUri2 == null) {
                        imageUri2 = "Exception: No Image Applied";
                    }
                    if (imageUri3 == null) {
                        imageUri3 = "Exception: No Image Applied";
                    }
                    if (imageUri4 == null) {
                        imageUri4 = "Exception: No Image Applied";
                    }
                    if (imageUri5 == null) {
                        imageUri5 = "Exception: No Image Applied";
                    }
                    Card card = new Card(title.getText().toString(), today, imageUri1, imageUri2,
                            imageUri3, imageUri4, imageUri5, content.getText().toString());
                    if (isConnected(getBaseContext())) {
                        Uri uri1 = Uri.parse(card.getImageResource1());
                        Uri uri2 = Uri.parse(card.getImageResource2());
                        Uri uri3 = Uri.parse(card.getImageResource3());
                        Uri uri4 = Uri.parse(card.getImageResource4());
                        Uri uri5 = Uri.parse(card.getImageResource5());
                        newResource = imageUri1;
                        if (!card.getImageResource1().equals("Exception: No Image Applied")) {
                            StorageReference path1 = storageRef.child("Photos/" + personId + "/" + title.getText() + uri1.getLastPathSegment());
                            UploadTask task = path1.putFile(uri1);
                            try {
                                /** Since the method is Asynchronous, we need to wait
                                 * until the method is fully processed
                                 */
                                Thread.sleep(4000);
                            } catch (Exception e) {

                            }
                            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    @SuppressWarnings("VisibleForTests") Uri uri = taskSnapshot.getMetadata().getDownloadUrl();
                                    if (uri.toString().startsWith("https://firebasestorage.googleapis.com")) {
                                        newResource = uri.toString();
                                    }
                                }
                            });
                        }
                        card.setImageResource1(newResource);
                        newResource = imageUri2;
                        if (!card.getImageResource2().equals("Exception: No Image Applied")) {
                            StorageReference path2 = storageRef.child("Photos/" + personId + "/" + title.getText() + uri2.getLastPathSegment());
                            UploadTask task = path2.putFile(uri2);
                            try {
                                /** Since the method is asynchronous,
                                 * we need to force the app to wait for a few seconds.
                                 */
                                Thread.sleep(4000);
                            } catch (Exception e) {

                            }
                            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    @SuppressWarnings("VisibleForTests") Uri uri = taskSnapshot.getMetadata().getDownloadUrl();
                                    if (uri.toString().startsWith("https://firebasestorage.googleapis.com")) {
                                        newResource = uri.toString();
                                    }
                                }
                            });
                        }
                        card.setImageResource2(newResource);
                        newResource = imageUri3;
                        if (!card.getImageResource3().equals("Exception: No Image Applied")) {
                            StorageReference path3 = storageRef.child("Photos/" + personId + "/" + title.getText() + uri3.getLastPathSegment());
                            UploadTask task = path3.putFile(uri3);
                            try {
                                Thread.sleep(4000);
                            } catch (Exception e) {

                            }
                            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    @SuppressWarnings("VisibleForTests") Uri uri = taskSnapshot.getMetadata().getDownloadUrl();
                                    if (uri.toString().startsWith("https://firebasestorage.googleapis.com")) {
                                        newResource = uri.toString();
                                    }
                                }
                            });
                        }
                        card.setImageResource3(newResource);
                        newResource = imageUri4;
                        if (!card.getImageResource4().equals("Exception: No Image Applied")) {
                            StorageReference path4 = storageRef.child("Photos/" + personId + "/" + title.getText() + uri4.getLastPathSegment());
                            UploadTask task = path4.putFile(uri4);
                            try {
                                Thread.sleep(4000);
                            } catch (Exception e) {

                            }
                            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    @SuppressWarnings("VisibleForTests") Uri uri = taskSnapshot.getMetadata().getDownloadUrl();
                                    if (uri.toString().startsWith("https://firebasestorage.googleapis.com")) {
                                        newResource = uri.toString();
                                    }
                                }
                            });
                        }
                        card.setImageResource4(newResource);
                        newResource = imageUri5;
                        if (!card.getImageResource5().equals("Exception: No Image Applied")) {
                            StorageReference path5 = storageRef.child("Photos/" + personId + "/" + title.getText() + uri5.getLastPathSegment());
                            UploadTask task = path5.putFile(uri5);
                            try {
                                Thread.sleep(4000);
                            } catch (Exception e) {

                            }
                            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    @SuppressWarnings("VisibleForTests") Uri uri = taskSnapshot.getMetadata().getDownloadUrl();
                                    if (uri.toString().startsWith("https://firebasestorage.googleapis.com")) {
                                        newResource = uri.toString();
                                    }
                                }
                            });
                        }
                        card.setImageResource5(newResource);
                    }
                    bDBHelper.addPost(card);
                    cardAdapter.add(card);
                    cardAdapter.notifyDataSetChanged();
                    Toast.makeText(AddPostActivity.this, "Saved.", Toast.LENGTH_SHORT).show();
                    bf.onResume();
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Start a new activity for camera.
     */
    public void getCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String imageFileName = "voyage_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        cameraUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), imageFileName));

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
        startActivityForResult(takePictureIntent, POST_CAMERA);
    }

    // Get an image from the local gallery of the user
    private void getAlbum() {
        // call gallery
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, POST_ALBUM);
    }

    /**
     * Crop the image file then send the output.
     */
    public void cropImage() {
        Intent intent = new Intent("com.android.camera.action.CROP");

        intent.setDataAndType(cameraUri, "image/*");
        intent.putExtra("scale", true);
        intent.putExtra("output", cameraUri);

        startActivityForResult(intent, POST_CROP);
    }

    /**
     * Create a new image files if the user crops it.
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name, How to specify the folder name (Problem: DIRECTORY_DCIM , there exists
        // devices without path to DIRECTORY_PICTURE)
        String imageFileName = "voyage_" + String.valueOf(System.currentTimeMillis());
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Voyage/");
        File file = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPath = file.getAbsolutePath();
        return file;


        /* How to save to the top memory location without specifying a specific path and folder
        String imageFileName = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        File storageDir = new File(Environment.getExternalStorageDirectory(), imageFileName);
        currentPath = storageDir.getAbsolutePath();
        return storageDir;
        */
    }

    /**
     * Set the thumbnail of images.
     */
    private void setImage() {
        Bitmap[] bitmaps = {image1.getDrawingCache(), image2.getDrawingCache(), image3.getDrawingCache(),
                image4.getDrawingCache(), image5.getDrawingCache()};

        int bitmapIndex = -1;
        boolean nullCheck = true;
        for (int i = 0; i < bitmaps.length && nullCheck; i++) {
            if (bitmaps[i] == null) {
                bitmapIndex = i;
                nullCheck = false;
            }
        }

        if (selectedGlobal != -1) {
            bitmapIndex = selectedGlobal;
        }

        if (bitmapIndex == -1) {
            Toast.makeText(getApplicationContext(), "You cannot upload more than 5 pictures.", Toast.LENGTH_LONG).show();
        } else if (imageUri1 == null) {
            Picasso.with(getApplicationContext()).load(cameraUri).fit().centerCrop().into(image1);
            image1.setVisibility(View.VISIBLE);
            imageUri1 = cameraUri.toString();
        } else if (imageUri2 == null) {
            Picasso.with(getApplicationContext()).load(cameraUri).fit().centerCrop().into(image2);
            image2.setVisibility(View.VISIBLE);
            imageUri2 = cameraUri.toString();
        } else if (imageUri3 == null) {
            Picasso.with(getApplicationContext()).load(cameraUri).fit().centerCrop().into(image3);
            image3.setVisibility(View.VISIBLE);
            imageUri3 = cameraUri.toString();
        } else if (imageUri4 == null) {
            Picasso.with(getApplicationContext()).load(cameraUri).fit().centerCrop().into(image4);
            image4.setVisibility(View.VISIBLE);
            imageUri4 = cameraUri.toString();
        } else if (imageUri5 == null) {
            Picasso.with(getApplicationContext()).load(cameraUri).fit().centerCrop().into(image5);
            image5.setVisibility(View.VISIBLE);
            imageUri5 = cameraUri.toString();
        }
        selectedGlobal = -1;
    }

    /**
     * Customized menu bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_action_bar, menu);
        return true;
    }

    /**
     * Go to the previous activity(screen) once the user clicks back button.
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    /** Jaebin Yang */
    /** Check if the device is connected to the internet */
    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED;
    }
}
