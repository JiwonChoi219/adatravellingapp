package com.example.ada.voyage.budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ada.voyage.R;

import java.util.ArrayList;

/**
 * Created by: Jiyoon Lim
 * Description: customized adapter: get money from money list and connect its attributes into view
 */

public class MoneyAdapter extends BaseAdapter {
    private ArrayList<Money> moneyArrayList;
    private Context context;

    //constructor
    public MoneyAdapter(Context context, ArrayList<Money> moneyArrayList) {
        this.context = context;
        this.moneyArrayList = moneyArrayList;
    }

    //the number of stored items in mAdapter
    @Override
    public int getCount() {
        return moneyArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return moneyArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return moneyArrayList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.money_listview_item, parent, false);

            MoneyViewHolder holder = new MoneyViewHolder();
            holder.dateTextView = (TextView)convertView.findViewById(R.id.money_list_date);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.money_list_title);
            holder.amountTextView = (TextView) convertView.findViewById(R.id.money_list_amount);
            holder.signTextView = (TextView) convertView.findViewById(R.id.money_list_sign);
            convertView.setTag(holder);
        }

        Money money = moneyArrayList.get(position);
        if (money != null) {
            MoneyViewHolder holder = (MoneyViewHolder)convertView.getTag();
            holder.dateTextView.setText(money.getDate());
            holder.titleTextView.setText(money.getTitle());
            holder.amountTextView.setText(money.getAmount());
            holder.signTextView.setText(money.getSign());
        }

        return convertView;
    }

    public void add(Money money) {
        moneyArrayList.add(money);
    }

    public void edit(int position, Money money) {
        moneyArrayList.set(position, money);
    }

    public void clear() {
        moneyArrayList.clear();
    }

    class MoneyViewHolder {
        protected TextView dateTextView;
        protected TextView titleTextView;
        protected TextView amountTextView;
        protected TextView signTextView;
    }
}
