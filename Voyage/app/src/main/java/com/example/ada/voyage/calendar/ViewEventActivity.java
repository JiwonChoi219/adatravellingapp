package com.example.ada.voyage.calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import static com.example.ada.voyage.calendar.CalendarFragment.cDBHelper;
import static com.example.ada.voyage.calendar.CalendarFragment.eventAdapter;
import static com.example.ada.voyage.calendar.CalendarFragment.scheduleList;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: View event and can be navigated to "Edit" and "Delete".
*/

public class ViewEventActivity extends AppCompatActivity {
    private LinearLayout viewDescriptionSection;
    private TextView viewTitle, viewLocation, viewDescription, viewStartDate, viewStartTime, viewEndDate, viewEndTime;
    private CheckBox allDay;
    private int eventListIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity_view_event);

        /**
         * Customize the Activity's Action Bar
         */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Details");
        actionBar.setDisplayHomeAsUpEnabled(true);

        /**
         * Attribute declarations
         */
        viewDescriptionSection = (LinearLayout)findViewById(R.id.calendar_view_description_view);
        viewTitle = (TextView)findViewById(R.id.calendar_view_title);
        viewLocation = (TextView)findViewById(R.id.calendar_view_location);
        viewStartDate = (TextView)findViewById(R.id.calendar_view_start_date);
        viewStartTime = (TextView)findViewById(R.id.calendar_view_start_time);
        viewEndDate = (TextView)findViewById(R.id.calendar_view_end_date);
        viewEndTime = (TextView)findViewById(R.id.calendar_view_end_time);
        allDay = (CheckBox)findViewById(R.id.calendar_view_checkBox);
        viewDescription = (TextView)findViewById(R.id.calendar_view_description);

        /**
         * Get the information of the event clicked by the user in the Calendar Fragment
         */
        Intent intent = getIntent();
        viewTitle.setText(intent.getStringExtra("Schedule Title"));
        viewLocation.setText(intent.getStringExtra("Schedule Location"));
        viewStartDate.setText(intent.getStringExtra("Schedule Start Date"));
        viewStartTime.setText(intent.getStringExtra("Schedule Start Time"));
        viewEndDate.setText(intent.getStringExtra("Schedule End Date"));
        viewEndTime.setText(intent.getStringExtra("Schedule End Time"));
        viewDescription.setText(intent.getStringExtra("Schedule Description"));

        /**
         * If the user has selected the All Day option,
         * Set the allDay checkbox to the visible in a checked state,
         * And hide the textview that shows the beginning time and end time.
         * If the user has not selected the All Day option,
         * Set the allDay checkbox to not be visible.
         *
         * Other cases: print Toast message
         */
        if (intent.getStringExtra("Schedule All Day").equals("1")) {
            allDay.setChecked(true);
            viewStartTime.setVisibility(View.GONE);
            viewEndTime.setVisibility(View.GONE);
        } else if (intent.getStringExtra("Schedule All Day").equals("0")) {
            allDay.setVisibility(View.GONE);
        } else {
            Toast.makeText(this, "Error in database.", Toast.LENGTH_SHORT).show();
        }

        /**
         * If the user has not entered the description previously,
         * Do not show it in the layout.
         */
        if (viewDescription.getText().toString().equals("Exception: No Text Applied")) {
            viewDescriptionSection.setVisibility(View.GONE);
        }

        eventListIndex = intent.getIntExtra("Schedule List Index", 0);
    }



    /**
     * Add the customized menu bar to the class
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_calendar_detail_bar, menu);
        return true;
    }

    /**
     * When the user has selected edit or delete from the menu bar
     * @param item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         * @param Get the item's id
         */
        int id = item.getItemId();

        switch (id) {
            /**
             * When the user as selected the edit button.
             */

            case R.id.detail_bar_edit:
                /**
                 * Move on to the EditEventActivity class
                 */
                Intent i = new Intent(ViewEventActivity.this, EditEventActivity.class);
                Intent pass = getIntent();

                /**
                 * Give the information on the event to the EditEventActivity class.
                 */
                i.putExtra("Edit Schedule List Index", pass.getIntExtra("Schedule List Index", 0));
                i.putExtra("Edit Schedule ID", pass.getIntExtra("Schedule ID", -1));
                i.putExtra("Edit Schedule Title", pass.getStringExtra("Schedule Title"));
                i.putExtra("Edit Schedule Location", pass.getStringExtra("Schedule Location"));
                i.putExtra("Edit Schedule Start Date", pass.getStringExtra("Schedule Start Date"));
                i.putExtra("Edit Schedule Start Time", pass.getStringExtra("Schedule Start Time"));
                i.putExtra("Edit Schedule End Date", pass.getStringExtra("Schedule End Date"));
                i.putExtra("Edit Schedule End Time", pass.getStringExtra("Schedule End Time"));
                i.putExtra("Edit Schedule All Day", pass.getStringExtra("Schedule All Day"));
                i.putExtra("Edit Schedule Description", pass.getStringExtra("Schedule Description"));

                finish();
                startActivity(i);

                break;

            /**
             * When the user has selected the DELETE button,
             * Print a dialog that says the deletion has successfully complete.
             * Close ViewEventActivity             */
            case R.id.detail_bar_delete:
                /**
                 * Get the index of the information from the ArrayList eventList in order to delete it.
                 */
                Intent temp = getIntent();
                int position = temp.getIntExtra("Schedule List Index", 0);

                /**
                 * Delete from the Database
                 * Delte from the eventList
                 * Notify the eventAdapter of the change in data.
                 */
                cDBHelper.deleteEvent(scheduleList.get(position));
                scheduleList.remove(position);
                eventAdapter.notifyDataSetChanged();

                finish();
                Toast.makeText(ViewEventActivity.this, "Deleted.", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * When the user selects to go back, go back to the previous window.
     * In this case it is the CalendarFragment.
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

}
