package com.example.ada.voyage.currency;

import com.example.ada.voyage.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YongHo on 2017. 5. 23..
 */

public class CurrencyExchange {


    /**
     * base : EUR
     * date : 2017-05-22
     * rates : {"AUD":1.5049,"BGN":1.9558,"BRL":3.7029,"CAD":1.5174,"CHF":1.0911,"CNY":7.7457,"CZK":26.488,"DKK":7.4432,"GBP":0.86353,"HKD":8.7533,"HRK":7.4415,"HUF":308.65,"IDR":14926,"ILS":4.0269,"INR":72.557,"JPY":125.18,"KRW":1256.1,"MXN":20.964,"MYR":4.8395,"NOK":9.3723,"NZD":1.612,"PHP":55.863,"PLN":4.1927,"RON":4.5609,"RUB":63.742,"SEK":9.7895,"SGD":1.5581,"THB":38.642,"TRY":4.0027,"USD":1.1243,"ZAR":14.82}
     */

    /** Json format of Fixer.io API */

    private String base;
    private String date;
    private RatesBean rates;

    public List<Currency> getCurrencyList(){
        List<Currency> currencyList = new ArrayList<>();
        currencyList.add(new Currency("AUD", R.drawable.aud, rates.getAUD()));
        currencyList.add(new Currency("BGN", R.drawable.bgn, rates.getBGN()));
        currencyList.add(new Currency("BRL", R.drawable.brl, rates.getBRL()));
        currencyList.add(new Currency("CAD", R.drawable.cad, rates.getCAD()));

        currencyList.add(new Currency("CHF", R.drawable.chf,rates.getCHF()));
        currencyList.add(new Currency("CNY", R.drawable.cny,rates.getCNY()));
        currencyList.add(new Currency("CZK", R.drawable.czk,rates.getCZK()));
        currencyList.add(new Currency("DKK", R.drawable.dkk,rates.getDKK()));

        currencyList.add(new Currency("GBP", R.drawable.gbp,rates.getGBP()));
        currencyList.add(new Currency("HKD", R.drawable.hkd,rates.getHKD()));
        currencyList.add(new Currency("HRK", R.drawable.hrk,rates.getHRK()));
        currencyList.add(new Currency("HUF", R.drawable.huf,rates.getHUF()));

        currencyList.add(new Currency("IDR", R.drawable.idr,rates.getIDR()));
        currencyList.add(new Currency("ILS", R.drawable.ils,rates.getILS()));
        currencyList.add(new Currency("INR", R.drawable.inr,rates.getINR()));
        currencyList.add(new Currency("JPY", R.drawable.jpy,rates.getJPY()));

        currencyList.add(new Currency("KRW", R.drawable.krw,rates.getKRW()));
        currencyList.add(new Currency("MXN", R.drawable.mxn,rates.getMXN()));
        currencyList.add(new Currency("MYR", R.drawable.myr,rates.getMYR()));
        currencyList.add(new Currency("NOK", R.drawable.nok,rates.getNOK()));

        currencyList.add(new Currency("NZD", R.drawable.nzd,rates.getNZD()));
        currencyList.add(new Currency("PHP", R.drawable.php,rates.getPHP()));
        currencyList.add(new Currency("PLN", R.drawable.pln,rates.getPLN()));
        currencyList.add(new Currency("RON", R.drawable.ron,rates.getRON()));

        currencyList.add(new Currency("RUB", R.drawable.rub,rates.getRUB()));
        currencyList.add(new Currency("SEK", R.drawable.sek,rates.getSEK()));
        currencyList.add(new Currency("SGD", R.drawable.sgd,rates.getSGD()));
        currencyList.add(new Currency("THB", R.drawable.thb,rates.getTHB()));

        currencyList.add(new Currency("TRY", R.drawable.trry, rates.getTRY()));
        currencyList.add(new Currency("USD", R.drawable.usd, rates.getUSD()));
        currencyList.add(new Currency("ZAR", R.drawable.zar, rates.getZAR()));

        return currencyList;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RatesBean getRates() {
        return rates;
    }

    public void setRates(RatesBean rates) {
        this.rates = rates;
    }

    public static class RatesBean {
        /**
         * AUD : 1.5049
         * BGN : 1.9558
         * BRL : 3.7029
         * CAD : 1.5174
         * CHF : 1.0911
         * CNY : 7.7457
         * CZK : 26.488
         * DKK : 7.4432
         * GBP : 0.86353
         * HKD : 8.7533
         * HRK : 7.4415
         * HUF : 308.65
         * IDR : 14926
         * ILS : 4.0269
         * INR : 72.557
         * JPY : 125.18
         * KRW : 1256.1
         * MXN : 20.964
         * MYR : 4.8395
         * NOK : 9.3723
         * NZD : 1.612
         * PHP : 55.863
         * PLN : 4.1927
         * RON : 4.5609
         * RUB : 63.742
         * SEK : 9.7895
         * SGD : 1.5581
         * THB : 38.642
         * TRY : 4.0027
         * USD : 1.1243
         * ZAR : 14.82
         */

        private double AUD;
        private double BGN;
        private double BRL;
        private double CAD;
        private double CHF;
        private double CNY;
        private double CZK;
        private double DKK;
        private double GBP;
        private double HKD;
        private double HRK;
        private double HUF;
        private double IDR;
        private double ILS;
        private double INR;
        private double JPY;
        private double KRW;
        private double MXN;
        private double MYR;
        private double NOK;
        private double NZD;
        private double PHP;
        private double PLN;
        private double RON;
        private double RUB;
        private double SEK;
        private double SGD;
        private double THB;
        private double TRY;
        private double USD;
        private double ZAR;

        public double getAUD() {
            return AUD;
        }

        public void setAUD(double AUD) {
            this.AUD = AUD;
        }

        public double getBGN() {
            return BGN;
        }

        public void setBGN(double BGN) {
            this.BGN = BGN;
        }

        public double getBRL() {
            return BRL;
        }

        public void setBRL(double BRL) {
            this.BRL = BRL;
        }

        public double getCAD() {
            return CAD;
        }

        public void setCAD(double CAD) {
            this.CAD = CAD;
        }

        public double getCHF() {
            return CHF;
        }

        public void setCHF(double CHF) {
            this.CHF = CHF;
        }

        public double getCNY() {
            return CNY;
        }

        public void setCNY(double CNY) {
            this.CNY = CNY;
        }

        public double getCZK() {
            return CZK;
        }

        public void setCZK(double CZK) {
            this.CZK = CZK;
        }

        public double getDKK() {
            return DKK;
        }

        public void setDKK(double DKK) {
            this.DKK = DKK;
        }

        public double getGBP() {
            return GBP;
        }

        public void setGBP(double GBP) {
            this.GBP = GBP;
        }

        public double getHKD() {
            return HKD;
        }

        public void setHKD(double HKD) {
            this.HKD = HKD;
        }

        public double getHRK() {
            return HRK;
        }

        public void setHRK(double HRK) {
            this.HRK = HRK;
        }

        public double getHUF() {
            return HUF;
        }

        public void setHUF(double HUF) {
            this.HUF = HUF;
        }

        public double getIDR() {
            return IDR;
        }

        public void setIDR(int IDR) {
            this.IDR = IDR;
        }

        public double getILS() {
            return ILS;
        }

        public void setILS(double ILS) {
            this.ILS = ILS;
        }

        public double getINR() {
            return INR;
        }

        public void setINR(double INR) {
            this.INR = INR;
        }

        public double getJPY() {
            return JPY;
        }

        public void setJPY(double JPY) {
            this.JPY = JPY;
        }

        public double getKRW() {
            return KRW;
        }

        public void setKRW(double KRW) {
            this.KRW = KRW;
        }

        public double getMXN() {
            return MXN;
        }

        public void setMXN(double MXN) {
            this.MXN = MXN;
        }

        public double getMYR() {
            return MYR;
        }

        public void setMYR(double MYR) {
            this.MYR = MYR;
        }

        public double getNOK() {
            return NOK;
        }

        public void setNOK(double NOK) {
            this.NOK = NOK;
        }

        public double getNZD() {
            return NZD;
        }

        public void setNZD(double NZD) {
            this.NZD = NZD;
        }

        public double getPHP() {
            return PHP;
        }

        public void setPHP(double PHP) {
            this.PHP = PHP;
        }

        public double getPLN() {
            return PLN;
        }

        public void setPLN(double PLN) {
            this.PLN = PLN;
        }

        public double getRON() {
            return RON;
        }

        public void setRON(double RON) {
            this.RON = RON;
        }

        public double getRUB() {
            return RUB;
        }

        public void setRUB(double RUB) {
            this.RUB = RUB;
        }

        public double getSEK() {
            return SEK;
        }

        public void setSEK(double SEK) {
            this.SEK = SEK;
        }

        public double getSGD() {
            return SGD;
        }

        public void setSGD(double SGD) {
            this.SGD = SGD;
        }

        public double getTHB() {
            return THB;
        }

        public void setTHB(double THB) {
            this.THB = THB;
        }

        public double getTRY() {
            return TRY;
        }

        public void setTRY(double TRY) {
            this.TRY = TRY;
        }

        public double getUSD() {
            return USD;
        }

        public void setUSD(double USD) {
            this.USD = USD;
        }

        public double getZAR() {
            return ZAR;
        }

        public void setZAR(double ZAR) {
            this.ZAR = ZAR;
        }
    }
}
