package com.example.ada.voyage.blog;

import android.content.Context;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description:
 */

public class Hashtag extends ClickableSpan {
    public static String hashtag = "";
    private Context context;
    private TextPaint textPaint;

    // Constructor
    public Hashtag(Context ctx) {
        super();
        context = ctx;
    }

    /**
     * Method for coloring hashtagged texts and setup link color (the color when the user clicks the word)
     * @param ds
     */
    @Override
    public void updateDrawState(TextPaint ds) {
        textPaint = ds;
        ds.setColor(ds.linkColor);
        ds.setARGB(255, 24, 81, 117);
    }

    /**
     * Allow the user to move on to the other Activity by setting up onClick (link).
     * @param widget
     */
    @Override
    public void onClick(View widget) {
        TextView tv = (TextView) widget;
        Spanned s = (Spanned) tv.getText();
        int start = s.getSpanStart(this);
        int end = s.getSpanEnd(this);
        String theWord = s.subSequence(start + 1, end).toString();
        hashtag = theWord;
        Toast.makeText(context, String.format("Search for tag: %s", theWord), Toast.LENGTH_SHORT).show();
    }
}
