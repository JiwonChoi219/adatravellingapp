
package com.example.ada.voyage.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by: Jiwon Choi
 * Modified by: Jaebin Yang
 * Description: SQLite Database for Calendar(schedule/event)
 */

public class CalendarDBHelper extends SQLiteOpenHelper {
    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database information
    private static final String DATABASE_NAME = "EventDB.db";

    // Table name
    private static final String TABLE_NAME = "ScheduleTable";

    // Table columns
    public static final String COLUMN_ID_CALENDAR = "_id";
    public static final String COLUMN_TITLE_CALENDAR = "title";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_START_DATE = "startDate";
    public static final String COLUMN_START_TIME = "startTime";
    public static final String COLUMN_END_DATE = "endDate";
    public static final String COLUMN_END_TIME = "endTime";
    public static final String COLUMN_ALL_DAY = "allDay";
    public static final String COLUMN_DESCRIPTION = "description";

    private static final String query = "CREATE TABLE " + TABLE_NAME + "(" +
            COLUMN_ID_CALENDAR + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_TITLE_CALENDAR + " TEXT, " + COLUMN_LOCATION + " TEXT, " + COLUMN_START_DATE + " TEXT, " + COLUMN_START_TIME
            + " TEXT, " + COLUMN_END_DATE + " TEXT, " + COLUMN_END_TIME + " TEXT, " + COLUMN_ALL_DAY + " TEXT, " +
            COLUMN_DESCRIPTION + " TEXT );";

    /**
     * Constructor
     * @param context
     * @param name
     * @param factory
     * @param version
    */
    public CalendarDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Open SQLite database
     * @param db
     */
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
    }

    /**
     * Upgrade the version of the database table
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Add Schedule: insert attributes (title, location, start date, ... etc.) of Schedule object
     * into database column variable
     * and close the database
     * Add Schedule: get Schedule object and get its attributes (title, location, start date, ... etc.)
     * insert those to database columns, then close the database.
     *
     * @param schedule
     */
    public void addEvent(Schedule schedule) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE_CALENDAR, schedule.getTitle());
        values.put(COLUMN_LOCATION, schedule.getLocation());
        values.put(COLUMN_START_DATE, schedule.getStartDate());
        values.put(COLUMN_START_TIME, schedule.getStartTime());
        values.put(COLUMN_END_DATE, schedule.getEndDate());
        values.put(COLUMN_END_TIME, schedule.getEndTime());
        values.put(COLUMN_ALL_DAY, schedule.getAllDay());
        values.put(COLUMN_DESCRIPTION, schedule.getDescription());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    /**
     * Edit Schedule: get Schedule object and get its attributes (title, location, start date, ... etc.)
     * overwrite those on database column, then close the database.
     * Get the id value from the object, then insert it into the proper column.
     *
     * @param schedule
     */
    public void editEvent(Schedule schedule) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE_CALENDAR, schedule.getTitle());
        values.put(COLUMN_LOCATION, schedule.getLocation());
        values.put(COLUMN_START_DATE, schedule.getStartDate());
        values.put(COLUMN_START_TIME, schedule.getStartTime());
        values.put(COLUMN_END_DATE, schedule.getEndDate());
        values.put(COLUMN_END_TIME, schedule.getEndTime());
        values.put(COLUMN_ALL_DAY, schedule.getAllDay());
        values.put(COLUMN_DESCRIPTION, schedule.getDescription());
        db.update(TABLE_NAME, values, COLUMN_ID_CALENDAR + "=" + schedule.getId(), null);
        db.close();
    }

    /**
     * The only difference from viewSchedule() method is that it gets events under a certain date.
     *
     * @param date
     * @return JSONArray array
     */
    public JSONArray viewDesignatedEvent(String date) throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE (" + COLUMN_START_DATE + " = '" + date + "');";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_ID_CALENDAR, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_TITLE_CALENDAR, cursor.getString(cursor.getColumnIndex(COLUMN_TITLE_CALENDAR)));
            object.put(COLUMN_LOCATION, cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)));
            object.put(COLUMN_START_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_START_DATE)));
            object.put(COLUMN_START_TIME, cursor.getString(cursor.getColumnIndex(COLUMN_START_TIME)));
            object.put(COLUMN_END_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_END_DATE)));
            object.put(COLUMN_END_TIME, cursor.getString(cursor.getColumnIndex(COLUMN_END_TIME)));
            object.put(COLUMN_ALL_DAY, cursor.getString(cursor.getColumnIndex(COLUMN_ALL_DAY)));
            object.put(COLUMN_DESCRIPTION, cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    /**
     * View All Schedule: insert every data to the JSONArray.
     * while loop: create a JSONObject then insert values to the object. After, put the object to the JSONArray.
     * Close the database after inserting it.
     *
     * @return JSONArray array
     */
    public JSONArray viewEvent() throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_ID_CALENDAR, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_TITLE_CALENDAR, cursor.getString(cursor.getColumnIndex(COLUMN_TITLE_CALENDAR)));
            object.put(COLUMN_LOCATION, cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)));
            object.put(COLUMN_START_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_START_DATE)));
            object.put(COLUMN_START_TIME, cursor.getString(cursor.getColumnIndex(COLUMN_START_TIME)));
            object.put(COLUMN_END_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_END_DATE)));
            object.put(COLUMN_END_TIME, cursor.getString(cursor.getColumnIndex(COLUMN_END_TIME)));
            object.put(COLUMN_ALL_DAY, cursor.getString(cursor.getColumnIndex(COLUMN_ALL_DAY)));
            object.put(COLUMN_DESCRIPTION, cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    /**
     * Delete Schedule: get the Schedule object and get its id value.
     * Compare the id value that the user is trying to delete.
     * Look for the id value, compare it with existing ids.
     * Once the id has been figured out, then delete the whole column from the database.
     *
     * @param schedule
     * @return JSONArray array
     */
    public void deleteEvent(Schedule schedule) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_ID_CALENDAR + " = " + schedule.getId() + ");";
        db.execSQL(query);
        db.close();
    }


    /** Jaebin Yang */
    /** Delete Event: delete all the events stored in the CalendarDBHelper.
     * This method will be used when receiving data from the Firebase.
     * Because the data in the local database will simply be overwritten by the
     * data stored in the firebase.
     */
    public void deleteAll() {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + TABLE_NAME);
            db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME + "'");
            db.close();
    }

}
