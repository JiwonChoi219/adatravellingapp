package com.example.ada.voyage.blog;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: This class contains elements for a card/blog post.
*/

public class Card {
    // Attributes that a "Card" object should contain.
    private int id;
    private String cardTitle;
    private String cardDate;
    private String imageResource1;
    private String imageResource2;
    private String imageResource3;
    private String imageResource4;
    private String imageResource5;
    private String cardContent;

    // Constructor with id
    public Card(int id, String cardTitle, String cardDate, String imageResource1, String imageResource2,
                String imageResource3, String imageResource4, String imageResource5, String cardContent) {
        this.id = id;
        this.cardTitle = cardTitle;
        this.cardDate = cardDate;
        this.imageResource1 = imageResource1;
        this.imageResource2 = imageResource2;
        this.imageResource3 = imageResource3;
        this.imageResource4 = imageResource4;
        this.imageResource5 = imageResource5;
        this.cardContent = cardContent;
    }

    // Constructor without id value
    public Card(String cardTitle, String cardDate, String imageResource1, String imageResource2,
                String imageResource3, String imageResource4, String imageResource5, String cardContent) {
        this.cardTitle = cardTitle;
        this.cardDate = cardDate;
        this.imageResource1 = imageResource1;
        this.imageResource2 = imageResource2;
        this.imageResource3 = imageResource3;
        this.imageResource4 = imageResource4;
        this.imageResource5 = imageResource5;
        this.cardContent = cardContent;
    }

    // Accessor and Mutator methods (Getters & Setters):
    public int getCardId() {
        return id;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    public String getCardDate() {
        return cardDate;
    }

    public void setCardDate(String cardDate) {
        this.cardDate = cardDate;
    }

    public String getImageResource1() {
        return imageResource1;
    }

    public void setImageResource1(String imageResource1) {
        this.imageResource1 = imageResource1;
    }

    public String getImageResource2() {
        return imageResource2;
    }

    public void setImageResource2(String imageResource2) {
        this.imageResource2 = imageResource2;
    }

    public String getImageResource3() {
        return imageResource3;
    }

    public void setImageResource3(String imageResource3) {
        this.imageResource3 = imageResource3;
    }

    public String getImageResource4() {
        return imageResource4;
    }

    public void setImageResource4(String imageResource4) {
        this.imageResource4 = imageResource4;
    }

    public String getImageResource5() {
        return imageResource5;
    }

    public void setImageResource5(String imageResource5) {
        this.imageResource5 = imageResource5;
    }

    public String getCardContent() {
        return cardContent;
    }

    public void setCardContent(String cardContent) {
        this.cardContent = cardContent;
    }


}
