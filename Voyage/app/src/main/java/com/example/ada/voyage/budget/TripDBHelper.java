package com.example.ada.voyage.budget;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.ada.voyage.budget.CategoryDBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by: Jiwon Choi
 * Modified by: Jiyoon Lim
 * Description: database of moneys with its attributes.
 */

public class TripDBHelper extends SQLiteOpenHelper {
    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database information
    private static final String DATABASE_NAME = "TripDB.db";

    // Table name
    private static final String TABLE_NAME = "TripTable";


    // Table columns
    public static final String COLUMN_TRIP_ID = "_id";
    public static final String COLUMN_TRIP_TITLE = "tripTitle";
    public static final String COLUMN_TRIP_LEFT = "tripLeft";
    public static final String COLUMN_TRIP_SPENT = "tripSpent";

    private static final String query = "CREATE TABLE " + TABLE_NAME + "(" +
            COLUMN_TRIP_ID + " TEXT, " + COLUMN_TRIP_TITLE + " TEXT, " +
            COLUMN_TRIP_LEFT + " TEXT, " + COLUMN_TRIP_SPENT + " TEXT );";

    public TripDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addTrip(Trip trip) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TRIP_ID, trip.getId());
        values.put(COLUMN_TRIP_TITLE, trip.getTitle());
        values.put(COLUMN_TRIP_LEFT, trip.getMoneyLeft());
        values.put(COLUMN_TRIP_SPENT, trip.getMoneySpent());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void editTrip(Trip trip) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TRIP_TITLE, trip.getTitle());
        values.put(COLUMN_TRIP_LEFT, trip.getMoneyLeft());
        values.put(COLUMN_TRIP_SPENT, trip.getMoneySpent());
        db.update(TABLE_NAME, values, COLUMN_TRIP_ID + "=" + trip.getId(), null);
        db.close();
    }

    public JSONArray viewTrip() throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_TRIP_ID, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_TRIP_TITLE, cursor.getString(cursor.getColumnIndex(COLUMN_TRIP_TITLE)));
            object.put(COLUMN_TRIP_LEFT, cursor.getString(cursor.getColumnIndex(COLUMN_TRIP_LEFT)));
            object.put(COLUMN_TRIP_SPENT, cursor.getString(cursor.getColumnIndex(COLUMN_TRIP_SPENT)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    public void deleteTrip(Trip trip) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_TRIP_ID + " = " + trip.getId() + ");";
        db.execSQL(query);
        db.close();
    }

    /** Jaebin Yang:
     * Delete all the Trip objects in the database.
     */
    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME;
        db.execSQL(query);
        db.close();
    }

}
