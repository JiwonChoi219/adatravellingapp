package com.example.ada.voyage.budget;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.ada.voyage.R;

import static com.example.ada.voyage.budget.CategoryDetailActivity.mAdapter;
import static com.example.ada.voyage.budget.CategoryDetailActivity.moneyDBHelper;
import static com.example.ada.voyage.budget.CategoryDetailActivity.moneyList;

public class ViewMoneyDetailActivity extends AppCompatActivity {

    TextView storedTitle;
    TextView storedDate;
    TextView storedAmount;
    RadioButton storedPositiveButton;
    RadioButton storedNegativeButton;
    String storedSign;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_money_detail);

        storedTitle = (TextView) findViewById(R.id.view_money_title);
        storedDate = (TextView) findViewById(R.id.view_money_date);
        storedAmount = (TextView) findViewById(R.id.view_money_amount);
        storedNegativeButton = (RadioButton) findViewById(R.id.view_money_negative_radiobutton);
        storedPositiveButton = (RadioButton) findViewById(R.id.view_money_postive_radiobutton);

        Intent intent = getIntent();
        position = intent.getIntExtra("money detail position", -1);
        Money money = moneyList.get(position);
        storedTitle.setText(money.getTitle());
        storedDate.setText(money.getDate());
        storedAmount.setText(money.getAmount());
        storedSign = money.getSign();
        if (storedSign.equals("+")) {
            storedPositiveButton.setChecked(true);
        } else {//storedSign.equals("-")
            storedNegativeButton.setChecked(true);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Details");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_calendar_detail_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.detail_bar_edit:

                Intent intent = new Intent(ViewMoneyDetailActivity.this, EditMoneyDetailActivity.class);
                Intent pass = getIntent();

                intent.putExtra("edit money id", pass.getIntExtra("money detail id", -1));
                intent.putExtra("edit money position", pass.getIntExtra("money detail position", -1));
                startActivity(intent);
                finish();
                break;

            case R.id.detail_bar_delete:
                Intent temp = getIntent();
                int position = temp.getIntExtra("money detail position", -1);

                moneyDBHelper.deleteMoney(moneyList.get(position));
                moneyList.remove(position);
                mAdapter.notifyDataSetChanged();
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
