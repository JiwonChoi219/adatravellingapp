package com.example.ada.voyage.weather;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ada.voyage.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class WeatherFragment extends Fragment {
    protected EditText et;
    protected Button btn;
    protected static URL url;
    protected static ListView mListview;
    protected static WeatherListViewAdapter mAdapter;
    protected static ArrayList<WeatherListViewItem> weatherList = new ArrayList<>();
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    String apiKey = "2b4813da4966710a9946dbe472a44de1";


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, null);

        et = (EditText) view.findViewById(R.id.weather_search_text);
        btn = (Button) view.findViewById(R.id.weather_search_btn);

        mAdapter = new WeatherListViewAdapter(getContext(), weatherList);
        mListview = (ListView) view.findViewById(R.id.weather_list);
        mListview.setAdapter(mAdapter);

        //automatically recommend places according to input
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                } catch (GooglePlayServicesRepairableException e) {

                } catch (GooglePlayServicesNotAvailableException e) {

                }
            }
        });

        //when clicking search button, display 5 days forecasts of a certain city on listview
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cityName = et.getText().toString();
                URL url = createURL(cityName);

                if(cityName.length()==0){
                    Toast.makeText(getActivity(), "enter a city name first", Toast.LENGTH_SHORT).show();
                } else{
                    if(url == null){
                        Toast.makeText(getActivity(), "Try again: invalid city name", Toast.LENGTH_SHORT).show();
                    } else {
                        GetWeatherTask getLocalWeatherTask = new GetWeatherTask();
                        getLocalWeatherTask.execute(url);
                    }
                }
            }

//            private String parseJsonData(BufferedReader bufferedReader) {
//                String result = null;
//                try{
//                    JsonParser jsonParser = new JsonParser();
//                    JsonObject jsonObject = (JsonObject)jsonParser.parse(bufferedReader);
//                    result = jsonObject.toString();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return result;
//            }
        });//end of search button
        return view;
    }

    // create openweathermap.org webURL
    private URL createURL(String city) {
        try {
            // create URL for a certain city with 7 days of daily forecasts.
            String urlString = "http://api.openweathermap.org/data/2.5/forecast/daily?appid="+apiKey+"&q="
                    + URLEncoder.encode(city, "UTF-8")+ "&mode=json&units=metric&cnt=7";
            return new URL(urlString);
        }
        catch (Exception e) {
            Toast.makeText(getActivity(), "Try again: invalid city name", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return null;
    }

    private class GetWeatherTask extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) params[0].openConnection();
                int response = connection.getResponseCode();

                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {

                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        //Toast.makeText(getActivity(), "IOException: enable to read weather data", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
                else {
                    //Toast.makeText(getActivity(), "connect error: enable to connect to OpenWeatherMap.org", Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e) {
              //  Toast.makeText(getActivity(), "connect error: enable to connect to OpenWeatherMap.org", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            finally {
                connection.disconnect(); // close the HttpURLConnection
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject weather) {
            convertJSONtoArrayList(weather); // repopulate weatherList
            mAdapter.notifyDataSetChanged(); // rebind to ListView
            mListview.smoothScrollToPosition(0); // scroll to top
        }
    }

    // create Weather from JSONObjec and clear old weather list for showing a new weather list
    // and add weather items with data from JSONarray
    private void convertJSONtoArrayList(JSONObject forecast) {
        mAdapter.clear();
        try {
            JSONArray list = forecast.getJSONArray("list");

            for (int i = 0; i < list.length(); ++i) {
                JSONObject day = list.getJSONObject(i);
                JSONObject temperatures = day.getJSONObject("temp");
                JSONObject weather =
                        day.getJSONArray("weather").getJSONObject(0);
                WeatherListViewItem weatherItem = new WeatherListViewItem(
                        day.getLong("dt"), // date/time timestamp
                        temperatures.getDouble("min"), // minimum temperature
                        temperatures.getDouble("max"), // maximum temperature
                        weather.getString("description"),
                        weather.getString("icon"));
                weatherList.add(weatherItem);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch(NullPointerException e) {
            Toast.makeText(getActivity(), "Try again: invalid city name", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.i("WeatherFragment", "Place: " + place.getName());
                et.setText(place.getName());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.i("WeatherFragment", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled operation.
            }
        }
    }

}
