package com.example.ada.voyage.currency;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.icu.text.DecimalFormat;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ada.voyage.R;

public class CurrencyActivity_2 extends AppCompatActivity {

    private TextView tvTitle, tvSubTitle, tvOutputName, tvOutputRate, tvInPutName;
    private TextView tvOutputNameTitle, tvInputNameTitle;

    private EditText etInput;
    private Button btnCalculate;

    private String currencyName1;
    private double currencyRate;

    private Button btExchange;

    Currency c;
    private ImageView selectFlagView, europeFlag;
    private int flagNumber;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_2);

        //tvTitle = (TextView) findViewById(R.id.tvTitle);
       tvSubTitle = (TextView) findViewById(R.id.tvSubTitle);

        /** Display fragment at the top of the activity */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Currency Converter");

        tvOutputName = (TextView) findViewById(R.id.tvOutputName);
        tvOutputRate = (TextView) findViewById(R.id.tvOutputRate);
        etInput = (EditText) findViewById(R.id.etInput);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);
        btExchange = (Button) findViewById(R.id.btExchange);
        tvInPutName = (TextView) findViewById(R.id.tvInputName);

        tvInputNameTitle = (TextView) findViewById(R.id.tvInputNameTitle) ;
        tvOutputNameTitle = (TextView) findViewById(R.id.tvOutputNameTitle);

        selectFlagView = (ImageView) findViewById(R.id.selectFlagView);
        europeFlag = (ImageView) findViewById(R.id.EuropeFlag);


        /** Receiving the selected country information from the first activity */
        final Intent intent = getIntent();
        currencyName1 = intent.getStringExtra("currency_inputName");
        currencyRate = intent.getDoubleExtra("currency_rate", 0);
        flagNumber = intent.getIntExtra("flag_number", 0);

        //Set images of flags depends on the user's selection
        if (flagNumber == R.drawable.aud){
            selectFlagView.setImageResource(R.drawable.aud);
        }
        else if (flagNumber == R.drawable.bgn){
            selectFlagView.setImageResource(R.drawable.bgn);
        }
        else if (flagNumber == R.drawable.brl){
            selectFlagView.setImageResource(R.drawable.brl);
        }
        else if (flagNumber == R.drawable.cad){
            selectFlagView.setImageResource(R.drawable.cad);
        }
        else if (flagNumber == R.drawable.chf){
            selectFlagView.setImageResource(R.drawable.chf);
        }
        else if (flagNumber == R.drawable.cny){
            selectFlagView.setImageResource(R.drawable.cny);
        }
        else if (flagNumber == R.drawable.czk){
            selectFlagView.setImageResource(R.drawable.czk);
        }
        else if (flagNumber == R.drawable.dkk){
            selectFlagView.setImageResource(R.drawable.dkk);
        }
        else if (flagNumber == R.drawable.gbp){
            selectFlagView.setImageResource(R.drawable.gbp);
        }
        else if (flagNumber == R.drawable.hkd){
            selectFlagView.setImageResource(R.drawable.hkd);
        }
        else if (flagNumber == R.drawable.hrk){
            selectFlagView.setImageResource(R.drawable.hrk);
        }
        else if (flagNumber == R.drawable.huf){
            selectFlagView.setImageResource(R.drawable.huf);
        }
        else if (flagNumber == R.drawable.idr){
            selectFlagView.setImageResource(R.drawable.idr);
        }
        else if (flagNumber == R.drawable.ils){
            selectFlagView.setImageResource(R.drawable.ils);
        }
        else if (flagNumber == R.drawable.inr){
            selectFlagView.setImageResource(R.drawable.inr);
        }
        else if (flagNumber == R.drawable.jpy){
            selectFlagView.setImageResource(R.drawable.jpy);
        }
        else if (flagNumber == R.drawable.krw){
            selectFlagView.setImageResource(R.drawable.krw);
        }
        else if (flagNumber == R.drawable.mxn){
            selectFlagView.setImageResource(R.drawable.mxn);
        }
        else if (flagNumber == R.drawable.myr){
            selectFlagView.setImageResource(R.drawable.myr);
        }
        else if (flagNumber == R.drawable.nok){
            selectFlagView.setImageResource(R.drawable.nok);
        }
        else if (flagNumber == R.drawable.nzd){
            selectFlagView.setImageResource(R.drawable.nzd);
        }
        else if (flagNumber == R.drawable.php){
            selectFlagView.setImageResource(R.drawable.php);
        }
        else if (flagNumber == R.drawable.pln){
            selectFlagView.setImageResource(R.drawable.pln);
        }
        else if (flagNumber == R.drawable.ron){
            selectFlagView.setImageResource(R.drawable.ron);
        }
        else if (flagNumber == R.drawable.rub){
            selectFlagView.setImageResource(R.drawable.rub);
        }
        else if (flagNumber == R.drawable.sek){
            selectFlagView.setImageResource(R.drawable.sek);
        }
        else if (flagNumber == R.drawable.sgd){
            selectFlagView.setImageResource(R.drawable.sgd);
        }
        else if (flagNumber == R.drawable.thb){
            selectFlagView.setImageResource(R.drawable.thb);
        }
        else if (flagNumber == R.drawable.trry){
            selectFlagView.setImageResource(R.drawable.trry);
        }
        else if (flagNumber == R.drawable.usd){
            selectFlagView.setImageResource(R.drawable.usd);
        }
        else if (flagNumber == R.drawable.zar){
            selectFlagView.setImageResource(R.drawable.zar);
        }

        /** Set text views in the second XML layout with the information received from the first activity */
        tvOutputNameTitle.setText(currencyName1.toUpperCase());
        tvSubTitle.setText("Rate 1: " + currencyRate);
        tvOutputName.setText(currencyName1.toUpperCase() + ": ");

        /** The button for exchange rates between two countries */
        btExchange.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String temp = "";
                String titleTemp = "";
                String inputTemp="";

                temp = tvInPutName.getText().toString();
                tvInPutName.setText(tvOutputName.getText().toString());
                tvOutputName.setText(temp);

                titleTemp = tvInputNameTitle.getText().toString();
                tvInputNameTitle.setText(tvOutputNameTitle.getText().toString());
                tvOutputNameTitle.setText(titleTemp);

                if (tvInputNameTitle.getText().equals("EUR")){
                    europeFlag.setImageResource(R.drawable.flag_of_europe);
                    selectFlagView.setImageResource(flagNumber);
                    tvSubTitle.setText("Rate 1: " + currencyRate);

                }
                if (tvOutputNameTitle.getText().equals("EUR")){
                    europeFlag.setImageResource(flagNumber);
                    tvSubTitle.setText("Rate 1: " + String.format("%.4g%n", 1 / currencyRate));
                    selectFlagView.setImageResource(R.drawable.flag_of_europe);
                }

                inputTemp = etInput.getText().toString();
                etInput.setText(tvOutputRate.getText().toString());
                tvOutputRate.setText(inputTemp);



            }
        });


        /** Button for calculating currency */
        btnCalculate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(etInput.getText().toString().length() == 0){
                    return;
                }

                double input;
                try{
                    input = Double.parseDouble(etInput.getText().toString());
                }
                catch (NumberFormatException e){
                    etInput.setText("");
                    return;
                }

                if (tvInputNameTitle.getText().equals("EUR")){
                    double output = input * currencyRate;
                    tvOutputRate.setText(""+output);
                }
                if (tvOutputNameTitle.getText().equals("EUR")){
                    double output = input / currencyRate;
                    tvOutputRate.setText(""+output);
                }

            }
        });

    }
}
