package com.example.ada.voyage.blog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by: Jiwon Choi
 * Modified by: Jaebin Yang
 * Description: SQLite Database for Blog
 */

public class BlogDBHelper extends SQLiteOpenHelper {
    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database information
    private static final String DATABASE_NAME = "BlogDB.db";

    // Table name
    private static final String TABLE_NAME = "PostsTable";

    // Table columns
    public static final String COLUMN_ID_BLOG = "_id";
    public static final String COLUMN_TITLE_BLOG = "title";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_IMG_1 = "firstImg";
    public static final String COLUMN_IMG_2 = "secondImg";
    public static final String COLUMN_IMG_3 = "thirdImg";
    public static final String COLUMN_IMG_4 = "fourthImg";
    public static final String COLUMN_IMG_5 = "fifthImg";
    public static final String COLUMN_CONTENT = "description";

    // Query
    private static final String query = "CREATE TABLE " + TABLE_NAME + "(" +
            COLUMN_ID_BLOG + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_IMG_1 + " TEXT, " +
            COLUMN_IMG_2 + " TEXT, " + COLUMN_IMG_3 + " TEXT, " + COLUMN_IMG_4 + " TEXT, " +
            COLUMN_IMG_5 + " TEXT, " + COLUMN_TITLE_BLOG + " TEXT, " + COLUMN_DATE + " TEXT, " +
            COLUMN_CONTENT + " TEXT );";

    // Constructor
    public BlogDBHelper (Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Open Database
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
    }

    // Upgrade the version of the database.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Add Post: get the Card object with its attributes (i.e. title, date, image path, content)
     * put those into the database then close the database.
     *
     * @param card
     */
    public void addPost (Card card) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_IMG_1, card.getImageResource1());
        values.put(COLUMN_IMG_2, card.getImageResource2());
        values.put(COLUMN_IMG_3, card.getImageResource3());
        values.put(COLUMN_IMG_4, card.getImageResource4());
        values.put(COLUMN_IMG_5, card.getImageResource5());
        values.put(COLUMN_TITLE_BLOG, card.getCardTitle());
        values.put(COLUMN_DATE, card.getCardDate());
        values.put(COLUMN_CONTENT, card.getCardContent());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    /**
     * Edit Post: get the Card object, then overwrite attributes (title, date, image path, content)
     * to the database column variables. Close the database after overwriting.
     * Update/Overwrite: get the id value(column index) of the blog then overwrite values on the column.
     *
     * @param card
     */
    public void editPost (Card card) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE_BLOG, card.getCardTitle());
        values.put(COLUMN_DATE, card.getCardDate());
        values.put(COLUMN_IMG_1, card.getImageResource1());
        values.put(COLUMN_IMG_2, card.getImageResource2());
        values.put(COLUMN_IMG_3, card.getImageResource3());
        values.put(COLUMN_IMG_4, card.getImageResource4());
        values.put(COLUMN_IMG_5, card.getImageResource5());
        values.put(COLUMN_CONTENT, card.getCardContent());

        db.update(TABLE_NAME, values, COLUMN_ID_BLOG + "=" + card.getCardId(), null);
        db.close();
    }

    /**
     * View All Post: insert every data to the JSONArray.
     * while loop: create a JSONObject then insert values to the object. After, put the object to the JSONArray.
     * Close the database after inserting it.
     *
     * @return JSONArray array
     */
    public JSONArray viewPost() throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_ID_BLOG, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_TITLE_BLOG, cursor.getString(cursor.getColumnIndex(COLUMN_TITLE_BLOG)));
            object.put(COLUMN_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
            object.put(COLUMN_IMG_1, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_1)));
            object.put(COLUMN_IMG_2, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_2)));
            object.put(COLUMN_IMG_3, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_3)));
            object.put(COLUMN_IMG_4, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_4)));
            object.put(COLUMN_IMG_5, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_5)));
            object.put(COLUMN_CONTENT, cursor.getString(cursor.getColumnIndex(COLUMN_CONTENT)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    /**
     * The only difference from viewPost() method is that it only gets posts with a certain hashtag.
     *
     * @return JSONArray array
     */
    public JSONArray viewHashtag(String hashtag) throws JSONException {
        JSONArray array = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE (" + COLUMN_CONTENT + " LIKE '%" + hashtag + "%');";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put(COLUMN_ID_BLOG, cursor.getInt(cursor.getColumnIndex("_id")));
            object.put(COLUMN_TITLE_BLOG, cursor.getString(cursor.getColumnIndex(COLUMN_TITLE_BLOG)));
            object.put(COLUMN_DATE, cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
            object.put(COLUMN_IMG_1, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_1)));
            object.put(COLUMN_IMG_2, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_2)));
            object.put(COLUMN_IMG_3, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_3)));
            object.put(COLUMN_IMG_4, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_4)));
            object.put(COLUMN_IMG_5, cursor.getString(cursor.getColumnIndex(COLUMN_IMG_5)));
            object.put(COLUMN_CONTENT, cursor.getString(cursor.getColumnIndex(COLUMN_CONTENT)));
            array.put(object);
            cursor.moveToNext();
        }
        db.close();
        return array;
    }

    /**
     * Delete Post: get the Card object and get its id value.
     * Compare the id value that the user is trying to delete.
     * Look for the id value, compare it with existing ids.
     * Once the id has been figured out, then delete the whole column from the database.
     *
     * @param card
     * @return JSONArray array
     */
    public void deletePost(Card card) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM "+ TABLE_NAME + " WHERE (" + COLUMN_ID_BLOG +" = " + card.getCardId() + ");";
        db.execSQL(query);
        db.close();
    }

    /**
     * Created by: Jaebin Yang
     * delete every posts in the database to sync with the data in Firebase.
     * We will sync the data by overwriting the data in the local database
     * with the data in the Firebase.
     */
    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME + "'");
        db.close();
    }

}
