package com.example.ada.voyage.currency;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//This class is mainly for implementing CurrencyActivity_2

public class CurrencyActivity extends AppCompatActivity implements Callback<CurrencyExchange>, CurrencyItemClickListener {

    Context context;
    private ListView lvCurrency;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        setTitle("Currency Converter");
        getMenuInflater().inflate(R.menu.custom_cancel_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bar_cancel:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
        lvCurrency = (ListView) findViewById(R.id.lvcurrency);
    }


    @Override
    protected void onStart() {
        super.onStart();
        loadCurrencyExchangeData();
    }

    /** Call Fixor.io api to get recent currency exchange rates */
    private void loadCurrencyExchangeData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CurrencyExchangeService service = retrofit.create(CurrencyExchangeService.class);
        Call<CurrencyExchange> call = service.loadCurrencyExchange();
        call.enqueue(this);
    }

    /** List view of countries adapts CurrencyAdapter's defined settings */
    @Override
    public void onResponse(Call<CurrencyExchange> call, Response<CurrencyExchange> response) {
        //Toast.makeText(this, response.body().getBase(), Toast.LENGTH_LONG).show();
        CurrencyExchange currencyExchange = response.body();
        lvCurrency.setAdapter(new CurrencyAdapter(this, currencyExchange.getCurrencyList(), this));
    }

    @Override
    public void onFailure(Call<CurrencyExchange> call, Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_LONG).show();
    }

    /** Delivering the selected country's name, rate, flag information to CurrencyActivity_2 */
    @Override
    public void onCurrencyItemClick(Currency c, View view) {
        Intent intent = new Intent(this, CurrencyActivity_2.class);
        intent.putExtra("currency_inputName", c.getName1());
        intent.putExtra("currency_rate", c.getRate());
        intent.putExtra("flag_number", c.getFlag());
        startActivity(intent);

    }

}
