package com.example.ada.voyage.weather;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class WeatherListViewItem {
    public final String dayStr;
    public final String maxTempStr;
    public final String minTempStr;
    public final String descriptionStr;
    public final String iconStr;

    /**e.g. "dt":1497150000, "min":16.49,"max":20.48, "description":"sky is clear", "icon":"01d"
    * constructor which consist of data from JSON
    */
    public WeatherListViewItem(long timeStamp, double minTemp, double maxTemp, String description, String icon){

        /*
        * convert date value to name of day (e.g. 170303-->Monday)
        */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        TimeZone tz = TimeZone.getDefault();
        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
        /*returns the day's name*/
        SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE");
        this.dayStr = dateFormatter.format(calendar.getTime()) +" :";
        /*celsius temperatures: double temp + symbol*/
        this.minTempStr = minTemp + "\u00B0C";
        this.maxTempStr = maxTemp + "\u00B0C";
        this.descriptionStr = description;
        /* icon url from openweathermap*/
        this.iconStr = "http://openweathermap.org/img/w/" + icon + ".png";
    }//end of constructor
}