package com.example.ada.voyage.calendar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ada.voyage.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.example.ada.voyage.blog.BlogDBHelper.COLUMN_DATE;
import static com.example.ada.voyage.blog.BlogFragment.bDBHelper;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_ALL_DAY;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_DESCRIPTION;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_END_DATE;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_END_TIME;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_ID_CALENDAR;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_LOCATION;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_START_DATE;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_START_TIME;
import static com.example.ada.voyage.calendar.CalendarDBHelper.COLUMN_TITLE_CALENDAR;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description:
*/

public class CalendarFragment extends Fragment {
    // Calendar Library
    protected static CompactCalendarView compactCalendar;

    // Date format for today and current month
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
    private static SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM (yyyy)", Locale.ENGLISH);
    private TextView month;
    protected static Date passDate;

    protected static CalendarDBHelper cDBHelper;
    protected static ArrayList<Schedule> scheduleList = new ArrayList<>();
    protected static EventAdapter eventAdapter;
    protected static ListView listView;
    protected static List<Event> calendarList = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        setHasOptionsMenu(true);

        compactCalendar = (CompactCalendarView) view.findViewById(R.id.calendar_view);
        compactCalendar.setLocale(TimeZone.getDefault(), Locale.ENGLISH);
        compactCalendar.setUseThreeLetterAbbreviation(true);
        compactCalendar.setFirstDayOfWeek(Calendar.SUNDAY);
        compactCalendar.shouldDrawIndicatorsBelowSelectedDays(true);
        month = (TextView) view.findViewById(R.id.calendar_month);
        month.setText(dateFormatMonth.format(compactCalendar.getFirstDayOfCurrentMonth()));

        cDBHelper = new CalendarDBHelper(this.getActivity(), null, null, 1);
        listView = (ListView) view.findViewById(R.id.event_list);
        eventAdapter = new EventAdapter(getContext(), scheduleList);
        listView.setAdapter(eventAdapter);

        long currentTime = System.currentTimeMillis();
        Date today = new Date(currentTime);
        passDate = today;

        /**
         * When a date is selected in the calendar
         * Whenever the CalendarFragment class is running(Including situations where the users re-enters the fragment),
         * use the viewEvent() method to read the data in a corresponding JSONArray (viewEvent return value = JSONArray)
         * and connect it to the eventAdapter.
         * Show the elements in the eventAdapter in the listView
         * Set the date as the date the user selected.
         */
        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                passDate = dateClicked;
                eventAdapter.clear();
                try {
                    JSONArray array = cDBHelper.viewDesignatedEvent(dateFormatForDisplaying.format(dateClicked));
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = (JSONObject) array.get(i);
                        eventAdapter.add(new Schedule(obj.getInt(COLUMN_ID_CALENDAR),
                                obj.get(COLUMN_TITLE_CALENDAR).toString(),
                                obj.get(COLUMN_LOCATION).toString(),
                                obj.get(COLUMN_START_DATE).toString(),
                                obj.get(COLUMN_START_TIME).toString(),
                                obj.get(COLUMN_END_DATE).toString(),
                                obj.get(COLUMN_END_TIME).toString(),
                                obj.get(COLUMN_ALL_DAY).toString(),
                                obj.get(COLUMN_DESCRIPTION).toString()
                        ));
                    }
                    eventAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Failed to load database", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                month.setText(dateFormatMonth.format(firstDayOfNewMonth));
            }
        });

        /**
         * When the user clicks one of the events that are visible in the listview.
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /**
                 * Move on to the ViewEventActivity class
                 */
                Intent intent = new Intent(CalendarFragment.this.getActivity(), ViewEventActivity.class);

                /**
                 * Give the selected event information to the ViewEventActivity class
                 */
                intent.putExtra("Schedule List Index", position);
                intent.putExtra("Schedule ID", scheduleList.get(position).getId());
                intent.putExtra("Schedule Title", scheduleList.get(position).getTitle());
                intent.putExtra("Schedule Location", scheduleList.get(position).getLocation());
                intent.putExtra("Schedule Start Date", scheduleList.get(position).getStartDate());
                intent.putExtra("Schedule Start Time", scheduleList.get(position).getStartTime());
                intent.putExtra("Schedule End Date", scheduleList.get(position).getEndDate());
                intent.putExtra("Schedule End Time", scheduleList.get(position).getEndTime());
                intent.putExtra("Schedule All Day", scheduleList.get(position).getAllDay());
                intent.putExtra("Schedule Description", scheduleList.get(position).getDescription());

                startActivity(intent);
            }
        });

        // ITEM LONG CLICK DIALOG:
//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                cDBHelper.deleteEvent(scheduleList.get(position).getTitle());
//                scheduleList.remove(position);
//                eventAdapter.notifyDataSetChanged();
//                Toast.makeText(getContext(),"Deleted.",Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        });

        return view;
    }

    /**
     * Add a customized menu bar to the Calendar Fragment.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.custom_fragment_bar, menu);
    }

    /**
     * When the user selects the add button (+) in the menu bar, move on to the window that adds a new event.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bar_add:
                Intent intent = new Intent(CalendarFragment.this.getActivity(), AddEventActivity.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Whenever the CalendarFragment class is running (Including situations where the users re-enters the class),
     * use the viewEvent() method to read JSONArray data (viewEvent return value = JSONArray) and connect them to the eventAdapter.
     * use the eventAdapter to show the events on the listview
     * The date is saved as the current date.
     */
    @Override
    public void onResume() {
        super.onResume();

        long currentTime = System.currentTimeMillis();
        Date today = new Date(currentTime);

        eventAdapter.clear();
        try {
            JSONArray array = cDBHelper.viewDesignatedEvent(dateFormatForDisplaying.format(today));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                eventAdapter.add(new Schedule(obj.getInt(COLUMN_ID_CALENDAR),
                        obj.get(COLUMN_TITLE_CALENDAR).toString(),
                        obj.get(COLUMN_LOCATION).toString(),
                        obj.get(COLUMN_START_DATE).toString(),
                        obj.get(COLUMN_START_TIME).toString(),
                        obj.get(COLUMN_END_DATE).toString(),
                        obj.get(COLUMN_END_TIME).toString(),
                        obj.get(COLUMN_ALL_DAY).toString(),
                        obj.get(COLUMN_DESCRIPTION).toString()
                ));
            }
            eventAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Failed to load database", Toast.LENGTH_LONG).show();
        }

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy, HH:mm");
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        try {
            calendarList.clear();
            compactCalendar.removeAllEvents();
            JSONArray array = cDBHelper.viewEvent();
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                String startDate = obj.get(COLUMN_START_DATE).toString();
                String startTime = (obj.get(COLUMN_START_TIME).toString()).substring(0, 5);
                String mTime = startDate + ", " + startTime;
                Date date = null;
                try {
                    date = format.parse(mTime);
                    long milliTime = date.getTime();
                    calendarList.add(new Event(Color.parseColor("#5F99AF"), milliTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

            array = bDBHelper.viewPost();
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                String startDate = obj.get(COLUMN_DATE).toString();
                Date date = null;
                try {
                    date = formatter.parse(startDate);
                    long milliTime = date.getTime();
                    calendarList.add(new Event(Color.parseColor("#FF898F"), milliTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < calendarList.size(); i++) {
            compactCalendar.addEvent(calendarList.get(i));
        }
    }


}
