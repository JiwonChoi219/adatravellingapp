package com.example.ada.voyage.budget;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ada.voyage.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.ada.voyage.budget.BudgetFragment.moneyDBHelper;
import static com.example.ada.voyage.budget.BudgetFragment.tripDBHelper;
import static com.example.ada.voyage.budget.BudgetFragment.tripList;
import static com.example.ada.voyage.budget.CategoryDBHelper.COLUMN_CATEGORY_ID;
import static com.example.ada.voyage.budget.CategoryDBHelper.COLUMN_CATEGORY_LEFT;
import static com.example.ada.voyage.budget.CategoryDBHelper.COLUMN_CATEGORY_SPENT;
import static com.example.ada.voyage.budget.CategoryDBHelper.COLUMN_CATEGORY_TITLE;
import static com.example.ada.voyage.budget.CategoryDBHelper.COLUMN_CATEGORY_TRIP_ID;

/**
 * Created by: Jiyoon Lim
 * Modified by: Jiwon Choi (DB)
 * Description: Displays the list of categories that the user added.
 * The user can add, edit or remove the category
 * A total money spent and left are calculated
 */

public class TripDetailActivity extends AppCompatActivity {

    protected static ListView listView;
    protected static CategoryAdapter mAdapter;
    protected static CategoryDBHelper categoryDBHelper;
    protected static MoneyDBHelper moneyDBHelper;
    protected static ArrayList<Category> categoryList = new ArrayList<>();
    private TextView tripTitleTextView, tripLeftTextView, tripSpentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_detail);

        Intent intent = getIntent();
        int position = intent.getIntExtra("trip detail position", -1);
        Trip trip = tripList.get(position);

        tripTitleTextView = (TextView) findViewById(R.id.budget_title);
        tripLeftTextView = (TextView) findViewById(R.id.budget_total_money_left);
        tripSpentTextView = (TextView) findViewById(R.id.budget_total_money_spent);

        mAdapter = new CategoryAdapter(TripDetailActivity.this, categoryList);
        /*디비작업*/
        categoryDBHelper = new CategoryDBHelper(TripDetailActivity.this, null, null, 1);
        moneyDBHelper = new MoneyDBHelper(TripDetailActivity.this, null, null, 1);

        listView = (ListView) findViewById(R.id.budget_list);
        listView.setAdapter(mAdapter);
        // end of listView

        tripTitleTextView.setText(trip.getTitle());
        tripLeftTextView.setText(trip.getMoneyLeft());
        tripSpentTextView.setText(trip.getMoneySpent());

        //category list
        //when clicking a certain category, it goes view category detail
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, final int position, long id) {
                Category category = categoryList.get(position);

                Intent intent = new Intent(TripDetailActivity.this, CategoryDetailActivity.class);

                intent.putExtra("category detail id", category.getId());
                intent.putExtra("category detail position", position);
                startActivity(intent);
            }
        });

        //edit or remove the category
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder editRemoveCategory = new AlertDialog.Builder(TripDetailActivity.this);
                editRemoveCategory.setTitle("Edit or remove the category title");

                //editText
                final EditText et = new EditText(TripDetailActivity.this);
                et.setText(categoryList.get(position).getTitle());
                editRemoveCategory.setView(et);//end of editText

                editRemoveCategory.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (et.getText().toString().length() == 0) {
                            Toast.makeText(TripDetailActivity.this, "Enter a category title first", Toast.LENGTH_SHORT).show();
                        } else {
                            Category category = categoryList.get(position);
                            category.setTitle(et.getText().toString());
                            categoryDBHelper.editCategory(category);
                            categoryList.set(position, category);
                            mAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }
                });
                editRemoveCategory.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                editRemoveCategory.setNegativeButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Category category = categoryList.get(position);
                        moneyDBHelper.deleteMoneyFromCategory(category.getId());
                        categoryDBHelper.deleteCategory(category);
                        categoryList.remove(position);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                editRemoveCategory.show();
                return true;
            }
        });

        //end of clicking an item

        // actionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Trip Details");
        actionBar.setDisplayHomeAsUpEnabled(true);
        //end of actionBar

        //When clicking a floating button to add a new category
        FloatingActionButton addCategoryButton;
        addCategoryButton = (FloatingActionButton) findViewById(R.id.budget_add_btn);
        addCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //alertDialog to add a new trip on the list
                AlertDialog.Builder addCategory = new AlertDialog.Builder(TripDetailActivity.this);
                addCategory.setTitle("Add a new category");
                final EditText et = new EditText(TripDetailActivity.this);
                addCategory.setView(et); //end of editText
                addCategory.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (et.getText().toString().length() == 0) {
                            Toast.makeText(TripDetailActivity.this, "Enter a category title first", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            int tempCategoryId = -1;
                            Intent intent = getIntent();
                            int tripID = intent.getIntExtra("trip detail id", -1);

                            try {
                                JSONArray array = categoryDBHelper.viewCategory(tripID);
                                if (array.length() == 0) {
                                    tempCategoryId = 0;
                                } else {
                                    JSONObject obj = (JSONObject) array.get(array.length() - 1);
                                    tempCategoryId = obj.getInt(COLUMN_CATEGORY_ID) + 1;
                                }
                            } catch (JSONException e) {
                                Toast.makeText(TripDetailActivity.this, "Failed to load database", Toast.LENGTH_LONG).show();
                            }
                            Category category = new Category (tripID, et.getText().toString(), "0", "0");
                            categoryDBHelper.addCategory(category);
                            categoryList.add(category);
                            mAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }
                });
                addCategory.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                addCategory.show();
            }
        }); //end of floating button
    } // end onCreate()

    //calutae a total money
    protected String calculateSpent(){
        int total = 0;
        String totalStr = "";
        for(int i = 0; i < mAdapter.getCount(); i++){
            Category c = (Category) mAdapter.getItem(i);
            String ms = c.getMoneySpent();
            int a = Integer.parseInt(ms);
            total += a;
        }
        totalStr = String.valueOf(total);
        return totalStr;
    }
    //calutae a total money
    protected String calculateLeft(){
        int total = 0;
        String totalStr = "";
        for(int i = 0; i < mAdapter.getCount(); i++){
            Category c = (Category) mAdapter.getItem(i);
            String ml = c.getMoneyLeft();
            int a = Integer.parseInt(ml);
            total += a;
        }
        totalStr = String.valueOf(total);
        return totalStr;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //get data from database and view them on the list
        Intent intent = getIntent();
        int tripId = intent.getIntExtra("trip detail id", -1);
        mAdapter.clear();
        try {
            JSONArray array = categoryDBHelper.viewCategory(tripId);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                mAdapter.add(new Category(obj.getInt(COLUMN_CATEGORY_ID),
                        obj.getInt(COLUMN_CATEGORY_TRIP_ID),
                        obj.getString(COLUMN_CATEGORY_TITLE),
                        obj.getString(COLUMN_CATEGORY_LEFT),
                        obj.getString(COLUMN_CATEGORY_SPENT)));
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Failed to load database", Toast.LENGTH_LONG).show();
        }

        int position = intent.getIntExtra("trip detail position", -1);
        Trip trip = tripList.get(position);
        trip.setMoneyLeft(calculateLeft());
        trip.setMoneySpent(calculateSpent());
        tripDBHelper.editTrip(tripList.get(position));

        tripLeftTextView.setText(trip.getMoneyLeft());
        tripSpentTextView.setText(trip.getMoneySpent());
    }

    //back button
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    } //end of <-
}
