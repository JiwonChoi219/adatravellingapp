package com.example.ada.voyage.calendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.ada.voyage.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.example.ada.voyage.calendar.CalendarFragment.cDBHelper;
import static com.example.ada.voyage.calendar.CalendarFragment.scheduleList;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: Add schedule and save the new schedule on local database.
*/

public class AddEventActivity extends AppCompatActivity {
    private Schedule schedule;

    private String isClicked = "0";
    private TextView startDate, startTime, endDate, endTime;
    private int year, month, day, hour, minute;
    private Boolean startDateClicked = false, startTimeClicked = false, endDateClicked = false, endTimeClicked = false;
    private EditText title, location, description;
    private CheckBox allDay;

    /**
     * Add the customized menu bar to this class.
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_action_bar, menu);
        return true;
    }

    /**
     * Once the user clicks "CANCEL" or "SAVE" button on top:
     * @param item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         * @param item get the id
         */
        int id = item.getItemId();

        switch (id) {
            /**
             * Finish AddEventActivity
             */
            case R.id.action_bar_cancel:
                finish();
                break;

            /**
             * When "Save" button clicked:
             */
            case R.id.action_bar_save:
                /**
                 * Check whether the field is empty or not.
                 * If one of the field is empty, print out the message to guide the user to fill the blank in.
                 */
                if (title.getText().length() == 0 ||
                        location.getText().length() == 0 ||
                        startDate.getText().toString().equals("MM/DD/YYYY") ||
                        endDate.getText().toString().equals("MM/DD/YYYY")) {
                    Toast.makeText(AddEventActivity.this, "Invalid input: Check your input field.", Toast.LENGTH_SHORT).show();
                }

                /**
                 * If the inputs are valid, save it to the local database,
                 * then print out the Toast message that it is successfully saved.
                 */
                else {
                    /**
                     * If the user did not fill in the "Description" field,
                     * then put random text to this field.
                     */
                    if (description.getText().length() == 0) {
                        description.setText("Exception: No Text Applied");
                    }

                    /**
                     * Get the texts that the user put it in to the edittext field.
                     */
                    String strTitle = title.getText().toString();
                    String strLocation = location.getText().toString();
                    String strStartDate = startDate.getText().toString();
                    String strStartTime = startTime.getText().toString();
                    String strEndDate = endDate.getText().toString();
                    String strEndTime = endTime.getText().toString();
                    String strDescription = description.getText().toString();

                    /**
                     * Create Schedule object, add it to the database and ArrayList.
                     */
                    schedule = new Schedule(strTitle, strLocation, strStartDate, strStartTime, strEndDate, strEndTime, isClicked, strDescription);
                    cDBHelper.addEvent(schedule);
                    scheduleList.add(schedule);

                    Toast.makeText(AddEventActivity.this, "Saved.", Toast.LENGTH_SHORT).show();
                    finish();
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * "Go Back" to previous Activity/screen.
     * In this case, the user will move back to CalendarFragment.
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity_event);

        /**
         * Activity의 ActionBar customize
         */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Add Schedule");
        actionBar.setDisplayHomeAsUpEnabled(true);

        /**
         * Attribute declaration
         */
        cDBHelper = new CalendarDBHelper(this, null, null, 1);
        title = (EditText) findViewById(R.id.event_title);
        location = (EditText) findViewById(R.id.event_location);
        description = (EditText) findViewById(R.id.event_description);

        /**
         * GregorianCalendar & attributes: to setup Date Picker & Time Picker dialog.
         */
        GregorianCalendar calendar = new GregorianCalendar();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        /**
         * startDateClicked, startTimeClicked, endDateClicked, endTimeClicked boolean variables:
         * Flag attribute since they are reusing the Date || Time picker dialog.
         */
        startDate = (TextView) findViewById(R.id.event_start_date);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDateClicked = true;
                new DatePickerDialog(AddEventActivity.this, dateSetListener, year, month, day).show();
            }
        });

        startTime = (TextView) findViewById(R.id.event_start_time);
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimeClicked = true;
                new TimePickerDialog(AddEventActivity.this, timeSetListener, hour, minute, false).show();
            }
        });

        endDate = (TextView) findViewById(R.id.event_end_date);
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endDateClicked = true;
                new DatePickerDialog(AddEventActivity.this, dateSetListener, year, month, day).show();
            }
        });

        endTime = (TextView) findViewById(R.id.event_end_time);
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTimeClicked = true;
                new TimePickerDialog(AddEventActivity.this, timeSetListener, hour, minute, false).show();
            }
        });

        /**
         * Check whether the user clicked the "All Day" option or not.
         * If clicked, then set the time period from 00:00 AM to 11:59 PM.
         * String isClicked attribute -> save it to local database.
         */
        allDay = (CheckBox) findViewById(R.id.event_all_day);
        allDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isClicked = "1";
                    startTime.setText("00:00 AM");
                    endTime.setText("11:59 PM");
                    startTime.setVisibility(View.GONE);
                    endTime.setVisibility(View.GONE);
                } else {
                    isClicked = "0";
                    startTime.setText("08:00 AM");
                    endTime.setText("09:00 AM");
                    startTime.setVisibility(View.VISIBLE);
                    endTime.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    /**
     * Date Picker dialog
     */
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg = String.format("%d/%d/%d", monthOfYear + 1, dayOfMonth, year);

            if (monthOfYear < 10 || dayOfMonth < 10) {
                String month = "", day = "";

                if (monthOfYear < 10) {
                    month = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    month = String.valueOf(monthOfYear + 1);
                }

                if (dayOfMonth < 10) {
                    day = "0" + String.valueOf(dayOfMonth);
                } else {
                    day = String.valueOf(dayOfMonth);
                }

                msg = String.format("%s/%s/%d", month, day, year);
            }

            if (startDateClicked) {
                startDate.setText(msg);
                startDateClicked = false;
            } else if (endDateClicked) {
                endDate.setText(msg);
                endDateClicked = false;
            }
        }
    };

    /**
     * Time Picker Dialog
     */
    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String AM_PM;
            if (hourOfDay < 12) {
                AM_PM = "AM";
            } else {
                AM_PM = "PM";
                hourOfDay = hourOfDay - 12;
            }

            String msg = String.format("%d:%d %s", hourOfDay, minute, AM_PM);

            if (hourOfDay < 10 || minute < 10) {
                String hour = "", min = "";

                if (hourOfDay < 10) {
                    hour = "0" + String.valueOf(hourOfDay);
                } else {
                    hour = String.valueOf(hourOfDay);
                }

                if (minute < 10) {
                    min = "0" + String.valueOf(minute);
                } else {
                    min = String.valueOf(minute);
                }

                msg = String.format("%s:%s %s", hour, min, AM_PM);
            }

            if (startTimeClicked) {
                startTime.setText(msg);
                startTimeClicked = false;
            } else if (endTimeClicked) {
                endTime.setText(msg);
                endTimeClicked = false;
            }
        }

    };

}
