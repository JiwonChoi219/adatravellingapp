package com.example.ada.voyage.budget;

/**
 * Created by: Jiyoon Lim
 * Description: Money with id, id of trip/category which is upper hierarchy, title and date amount sign.
 */

public class Money {
    private int id;
    private int tripId;
    private int categoryId;
    private String title;
    private String date;
    private String amount;
    private String sign;

    public Money(int id, int tripId, int categoryId, String title, String date, String amount, String sign) {
        this.id = id;
        this.tripId = tripId;
        this.categoryId = categoryId;
        this.title = title;
        this.date = date;
        this.amount = amount;
        this.sign = sign;
    }

    public Money(int id, String title, String date, String amount, String sign) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.amount = amount;
        this.sign = sign;
    }

    public Money(String title, String date, String amount, String sign) {
        this.title = title;
        this.date = date;
        this.amount = amount;
        this.sign = sign;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
