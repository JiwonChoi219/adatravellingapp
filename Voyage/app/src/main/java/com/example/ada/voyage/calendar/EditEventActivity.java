package com.example.ada.voyage.calendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.ada.voyage.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.example.ada.voyage.calendar.CalendarFragment.cDBHelper;
import static com.example.ada.voyage.calendar.CalendarFragment.eventAdapter;
import static com.example.ada.voyage.calendar.CalendarFragment.scheduleList;
import static com.example.ada.voyage.calendar.CalendarFragment.listView;

/**
 * Created by: Jiwon Choi
 * Modified by:
 * Description: Edit schedule and save the changes in schedule on local database.
*/

public class EditEventActivity extends AppCompatActivity {
    private int eventListIndex;
    private Schedule schedule;
    private int eventID;

    private String isClicked;
    private TextView startDate, startTime, endDate, endTime;
    private int year, month, day, hour, minute;
    private Boolean startDateClicked = false, startTimeClicked = false, endDateClicked = false, endTimeClicked = false;
    private EditText title, location, description;
    private CheckBox allDay;

    /**
     * Add a customized menu bar to the class
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_action_bar, menu);
        return true;
    }

    /**
     * When the user selects CANCEL or SAVE to the menu bar
     * @param item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         * @param get the item's id
         */
        int id = item.getItemId();

        switch (id) {
            /**
             * When the user selects CANCEL, end AddEventActivity
             */
            case R.id.action_bar_cancel:
                finish();
                break;

            /**
             *  When the user has entered information and clicked SAVE
             */
            case R.id.action_bar_save:
                /**
                 * Get the index(position) of the event in the ArrayList eventList
                 * update the information in the eventList
                 */
                Intent intent = getIntent();
                int position = intent.getIntExtra("Edit Schedule List Index", 0);

                /**
                 * Check if the user hasn't left any fields as blank.
                 * If there are any blank fields, prompt the user to check.
                 */
                if (title.getText().length() == 0 ||
                        location.getText().length() == 0 ||
                        startDate.getText().toString().equals("MM/DD/YYYY") ||
                        endDate.getText().toString().equals("MM/DD/YYYY")) {
                    Toast.makeText(EditEventActivity.this, "Invalid input: Check your input field.", Toast.LENGTH_SHORT).show();
                }

                /**
                 * Save the data in the database if the user has entered valid information.
                 * Print a toast message that the data was properly saved, and close the Activity.
                 */
                else {
                    /**
                     * If the user has left the description field as blank,
                     * insert this text there instead.
                     */
                    if (description.getText().length() == 0) {
                        description.setText("Exception: No Text Applied");
                    }

                    /**
                     * If the user has not changed the all day option,
                     * return the previously saved data.
                     */
                    if (isClicked == null) {
                        isClicked = intent.getStringExtra("Edit Schedule All Day");
                    }

                    /**
                     * Get the values the user has entered in the fields.
                     */
                    String strTitle = title.getText().toString();
                    String strLocation = location.getText().toString();
                    String strStartDate = startDate.getText().toString();
                    String strStartTime = startTime.getText().toString();
                    String strEndDate = endDate.getText().toString();
                    String strEndTime = endTime.getText().toString();
                    String strDescription = description.getText().toString();

                    /**
                     * Make a new Schedule object from the information above.
                     * Update the database.
                     * Overwrite the information in the ArrayList.
                     * Notify the eventAdapter that data has been changed.
                     * Connect to the Adapter to show it in the listView
                     */
                    schedule = new Schedule(eventID, strTitle, strLocation, strStartDate, strStartTime,
                            strEndDate, strEndTime, isClicked, strDescription);
                    cDBHelper.editEvent(schedule);
                    scheduleList.set(position, schedule);
                    eventAdapter.notifyDataSetChanged();
                    listView.setAdapter(eventAdapter);

                    Toast.makeText(EditEventActivity.this, "Edited.", Toast.LENGTH_SHORT).show();
                    finish();
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * If the user chooses to go back, go back to the previous activity
     * Which would be the ViewEventActivity
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity_event);

        /**
         * Customize the Activity's ActionBar
         */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Edit Schedule");
        actionBar.setDisplayHomeAsUpEnabled(true);

        /**
         * Attribute declaration
         */
        cDBHelper = new CalendarDBHelper(this, null, null, 1);
        title = (EditText) findViewById(R.id.event_title);
        location = (EditText) findViewById(R.id.event_location);
        description = (EditText) findViewById(R.id.event_description);

        /**
         * Return the event to edit from the ViewEventActivity class
         */
        Intent intent = getIntent();

        /**
         * If the Schedule's id is smaller than 0,
         * Print an error message specifying an error in the database.
         */
        eventID = intent.getIntExtra("Edit Schedule ID", -1);
        if (eventID < 0) {
            Toast.makeText(this, "Error in database.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        /**
         * Show the previous data in the EditEventActivty window.        */
        title.setText(intent.getStringExtra("Edit Schedule Title"));
        location.setText(intent.getStringExtra("Edit Schedule Location"));

        /**
         * If the user did not enter anything the the description field previously,
         * Change the saved text ("Exception...") to null (empty)
         * If the user has any information stored there, show the information before editing.
         */
        if (intent.getStringExtra("Edit Schedule Description").equals("Exception: No Text Applied")) {
            description.setText("");
        } else {
            description.setText(intent.getStringExtra("Edit Schedule Description"));
        }

        /**
         * In order to update the information in the ArrayListeventList, get the event's index from the previous Activity
         */
        eventListIndex = intent.getIntExtra("Schedule List Index", 0);

        /**
         * GregorianCalendar & attributes: To set the Date Picker and Time Picker
         */
        GregorianCalendar calendar = new GregorianCalendar();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        /**
         * startDateClicked, startTimeClicked, endDateClicked, endTimeClicked boolean variables:
         * Wehn Date || Time Picker Dialog is executed, indicate where to enter the text         */
        startDate = (TextView) findViewById(R.id.event_start_date);
        startDate.setText(intent.getStringExtra("Edit Schedule Start Date"));
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDateClicked = true;
                new DatePickerDialog(EditEventActivity.this, dateSetListener, year, month, day).show();
            }
        });

        startTime = (TextView) findViewById(R.id.event_start_time);
        startTime.setText(intent.getStringExtra("Edit Schedule Start Time"));
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimeClicked = true;
                new TimePickerDialog(EditEventActivity.this, timeSetListener, hour, minute, false).show();
            }
        });

        endDate = (TextView) findViewById(R.id.event_end_date);
        endDate.setText(intent.getStringExtra("Edit Schedule End Date"));
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endDateClicked = true;
                new DatePickerDialog(EditEventActivity.this, dateSetListener, year, month, day).show();
            }
        });

        endTime = (TextView) findViewById(R.id.event_end_time);
        endTime.setText(intent.getStringExtra("Edit Schedule End Time"));
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTimeClicked = true;
                new TimePickerDialog(EditEventActivity.this, timeSetListener, hour, minute, false).show();
            }
        });

        allDay = (CheckBox) findViewById(R.id.event_all_day);
        if (intent.getStringExtra("Edit Schedule All Day").equals("1")) {
            allDay.setChecked(true);
            startTime.setVisibility(View.GONE);
            endTime.setVisibility(View.GONE);
        } else if (intent.getStringExtra("Edit Schedule All Day").equals("0")) {
            allDay.setChecked(false);
        }

        /**
         * Check if the user has selected the All Day Option
         * If so, select the beginning time and end times as 00:00 to 11:59, respectively.
         * Save to database as String isClicked attribute
         */
        allDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Intent intent = getIntent();

                if (isChecked) {
                    isClicked = "1";
                    startTime.setText("00:00 AM");
                    endTime.setText("11:59 PM");
                    startTime.setVisibility(View.GONE);
                    endTime.setVisibility(View.GONE);
                } else {
                    isClicked = "0";
                    startTime.setText(intent.getStringExtra("Edit Schedule Start Time"));
                    endTime.setText(intent.getStringExtra("Edit Schedule End Time"));
                    startTime.setVisibility(View.VISIBLE);
                    endTime.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    /**
     * Dialog that chooses the date.
     */
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg = String.format("%d/%d/%d", monthOfYear + 1, dayOfMonth, year);

            if (monthOfYear < 10 || dayOfMonth < 10) {
                String month = "", day = "";

                if (monthOfYear < 10) {
                    month = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    month = String.valueOf(monthOfYear + 1);
                }

                if (dayOfMonth < 10) {
                    day = "0" + String.valueOf(dayOfMonth);
                } else {
                    day = String.valueOf(dayOfMonth);
                }

                msg = String.format("%s/%s/%d", month, day, year);
            }

            if (startDateClicked) {
                startDate.setText(msg);
                startDateClicked = false;
            } else if (endDateClicked) {
                endDate.setText(msg);
                endDateClicked = false;
            }
        }
    };

    /**
     * Dialog to choose the time
     */
    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String AM_PM;
            if (hourOfDay < 12) {
                AM_PM = "AM";
            } else {
                AM_PM = "PM";
                hourOfDay = hourOfDay - 12;
            }

            String msg = String.format("%d:%d %s", hourOfDay, minute, AM_PM);

            if (hourOfDay < 10 || minute < 10) {
                String hour = "", min = "";

                if (hourOfDay < 10) {
                    hour = "0" + String.valueOf(hourOfDay);
                } else {
                    hour = String.valueOf(hourOfDay);
                }

                if (minute < 10) {
                    min = "0" + String.valueOf(minute);
                } else {
                    min = String.valueOf(minute);
                }

                msg = String.format("%s:%s %s", hour, min, AM_PM);
            }

            if (startTimeClicked) {
                startTime.setText(msg);
                startTimeClicked = false;
            } else if (endTimeClicked) {
                endTime.setText(msg);
                endTimeClicked = false;
            }
        }

    };
}
