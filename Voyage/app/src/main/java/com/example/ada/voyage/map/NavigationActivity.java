package com.example.ada.voyage.map;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;


import com.example.ada.voyage.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import Navigations.DirectionFinder;
import Navigations.DirectionFinderListener;
import Navigations.Route;

public class NavigationActivity extends AppCompatActivity implements OnMapReadyCallback, DirectionFinderListener {

    private GoogleMap mMap;
    private Button btnFindPath;
    private EditText etOrigin;
    private EditText etDestination;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    private String evalue;
    private ZoomControls zoom;

    private GPSTracker gpsTracker;
    private Location mLocation;
    double latitude, longitude;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final String TAG = "NavigationActivity";

    /** What3words attributes */
    String apiKey = "DK3JI1UT";
    private String baseUrl = "https://api.what3words.com/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        /** Display fragment at the top of the activity */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Navigation");

        /** Zoom In & Out Function */
        zoom = (ZoomControls) findViewById(R.id.nav_zcZoom);

        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });

        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });


        /** GPS to load lat and lon */
        gpsTracker  = new GPSTracker(getApplicationContext());
        mLocation = gpsTracker.getLocation();
        latitude = mLocation.getLatitude();
        longitude = mLocation.getLongitude();

        /** Obtain the SupportMapFragment and get notified when the map is ready to be used. */
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnFindPath = (Button) findViewById(R.id.btnFindPath);
        etOrigin = (EditText) findViewById(R.id.etOrigin);
        etDestination = (EditText) findViewById(R.id.etDestination);

        btnFindPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });
    }

    //Set text view of origin and destination from the Autocomplete Intent Builder
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** If the user chooses to go back, simply finish the activity */
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Place place = PlaceAutocomplete.getPlace(this, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.i(TAG, "Place: " + place.getName());

                if (evalue == "1"){
                    etOrigin.setText(place.getName());
                    if (etOrigin.getText().toString().length() != 0){
                        etOrigin.getText().clear();
                        etOrigin.setText(place.getName());
                    }
                }
                else {
                    etDestination.setText(place.getName());
                    if (etDestination.getText().toString().length() != 0){
                        etDestination.getText().clear();
                        etDestination.setText(place.getName());
                    }
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                /** The user canceled operation. */
            }
        }
    }

    /** Reverse Geocoding using Google Maps API */
    private String getRoadName(String lat, String lng) throws JSONException {
        String roadName = "";
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=__LAT__,__LNG__&sensor=false";

        url = url.replaceAll("__LAT__", lat);
        url = url.replaceAll("__LNG__", lng);

        DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
        HttpGet httpget = new HttpGet(url);
        InputStream inputStream = null;
        String result = null;

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            result = sb.toString();
        } catch (Exception e) {
            // Handling Error
        }
        finally {
            try {
                if(inputStream != null)inputStream.close();
            }
            catch(Exception squish){}
        }

        JSONObject jObject = new JSONObject(result);
        JSONArray jArray = jObject.getJSONArray("results");
        if (jArray != null && jArray.length() > 0) {
            try {
                JSONObject oneObject = jArray.getJSONObject(0);
                // Pulling items from the array
                roadName = oneObject.getString("formatted_address");
            } catch (JSONException e) {
                // Oops
            }
        }
        return roadName;
    }


    /** Check if edit texts are not empty before search directions */
    private void sendRequest() {
        String origin = etOrigin.getText().toString();
        String destination = etDestination.getText().toString();
        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }


        try {
            boolean control3 = true;
            boolean control4 = false;
            boolean control7 = false;

            /** Handling variety of cases of the input name from the user */
            for (int s=0; s<origin.length(); s++){
                if (origin.charAt(s) == '.'){
                    control4 = true;
                    break;
                }
            }

            for (int r=0; r<destination.length(); r++){
                if (destination.charAt(r) == '.'){
                    control7 = true;
                    break;
                }
            }

            /** When input is actual names of locations */
            if (control4 == false && control7 == false){
                new DirectionFinder(this, origin, destination).execute();
            }

            if ((control4 == false && control7 == true) || (control4 == true && control7 == false) ){
                Toast.makeText(this, "Please enter the three word address!", Toast.LENGTH_SHORT).show();
            }


            if (control4 && control7){
            //When the inputs are both three word address
            for (int a = 0; a < origin.length(); a++) {
                if (origin.charAt(a) == '.') {
                    if (control3) {
                        control3 = false;

                        //What3Words (Receive lat, long)
                        String coordinateURL = new What3WordsRequest(baseUrl, apiKey).RequestPosition(origin);
                        String coordinateURL2 = new What3WordsRequest(baseUrl, apiKey).RequestPosition(destination);

                        boolean control1 = true;
                        boolean control2 = true;
                        String addressTemp = "";
                        String latoriTemp = "";
                        String longioriTemp = "";

                        String addressTemp2 = "";
                        String latdesTemp = "";
                        String longidesTemp = "";

                        for (int i = 0; i < coordinateURL.length(); i++) {
                            if (coordinateURL.charAt(i) == '[') {
                                for (int j = i; j < coordinateURL.length(); j++) {
                                    addressTemp = coordinateURL.substring(i, j);
                                }

                            }
                        }

                        for (int i = 0; i < coordinateURL2.length(); i++) {
                            if (coordinateURL2.charAt(i) == '[') {
                                for (int j = i; j < coordinateURL2.length(); j++) {
                                    addressTemp2 = coordinateURL2.substring(i, j);
                                }

                            }
                        }

                        //Extracting only latitude section from HTTP call
                        for (int k = 0; k < addressTemp.length(); k++) {
                            if (addressTemp.charAt(k) == '[') {
                                for (int t = k; t < addressTemp.length(); t++) {
                                    if (control1) {
                                        if (addressTemp.charAt(t) == ',') {
                                            latoriTemp = addressTemp.substring(1, t);
                                            control1 = false;
                                        }
                                    }
                                }
                                //Extracting only longitude section from HTTP call
                                for (int c = 0; c < addressTemp.length(); c++) {
                                    if (addressTemp.charAt(c) == ',') {
                                        for (int d = c; d < addressTemp.length(); d++) {
                                            if (control2) {
                                                if (addressTemp.charAt(d) == ']') {
                                                    control2 = false;
                                                    longioriTemp = addressTemp.substring(c + 1, d);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        boolean control5 = true;
                        boolean control6 = true;
                        for (int k = 0; k < addressTemp2.length(); k++) {
                            if (addressTemp2.charAt(k) == '[') {
                                for (int t = k; t < addressTemp2.length(); t++) {
                                    if (control5) {
                                        if (addressTemp2.charAt(t) == ',') {
                                            latdesTemp = addressTemp2.substring(1, t);
                                            control5 = false;
                                        }
                                    }
                                }
                                //Extracting only longitude section from HTTP call
                                for (int c = 0; c < addressTemp2.length(); c++) {
                                    if (addressTemp2.charAt(c) == ',') {
                                        for (int d = c; d < addressTemp2.length(); d++) {
                                            if (control6) {
                                                if (addressTemp2.charAt(d) == ']') {
                                                    control6 = false;
                                                    longidesTemp = addressTemp2.substring(c + 1, d);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        new DirectionFinder(this, getRoadName(latoriTemp, longioriTemp), getRoadName(latdesTemp, longidesTemp)).execute();
                    }
                }
            }

            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    /** Add a marker to current location and move the camera */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Current Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }

        }
    }


    /** Refresh markers if user clicks find direction button again */
    @Override
    public void onDirectionFinderStart() {
        //progressDialog = ProgressDialog.show(this, "Please wait.",
           //     "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    /** Display markers of origin and destination with direction routes */
    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
//        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            ((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            ((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

}
