package Navigations;

/**
 * Created by YongHo Son on 5/28/2017..
 */
public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
