package Navigations;

import java.util.List;

/**
 * Created by YongHo Son on 5/28/2017..
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
